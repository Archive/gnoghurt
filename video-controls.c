/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <gtk/gtk.h>
#include "controls.h"

typedef struct _VideoInfo VideoInfo;

struct _VideoInfo {
	VidServer_Server server;

	int brightness;
	int hue;
	int color;
	int contrast;
        int double_scan;
};

static void
set_pic_params (VideoInfo *info)
{
	CORBA_Environment ev;
	char *params;

	CORBA_exception_init(&ev);

	params = g_strdup_printf("%d %d %d %d %d", info->brightness, info->hue, info->color, info->contrast, info->double_scan);
	VidServer_Server_set_params (info->server, params, &ev);
	g_free (params);
}

static void
contrast_changed (GtkAdjustment *adj, VideoInfo *info)
{
	info->contrast = (adj->value * 65535)/100;
	set_pic_params(info);
}

static void
hue_changed (GtkAdjustment *adj, VideoInfo *info)
{
	info->hue = (adj->value * 65535)/100;
	set_pic_params(info);
}

static void
brightness_changed (GtkAdjustment *adj, VideoInfo *info)
{
	info->brightness = (adj->value * 65535)/100;
	set_pic_params(info);
}

static void
color_changed (GtkAdjustment *adj, VideoInfo *info)
{
	info->color = (adj->value * 65535)/100;
	set_pic_params(info);
}

static void
double_changed (GtkWidget *widget, VideoInfo *info)
{
	info->double_scan = GTK_TOGGLE_BUTTON(widget)->active;
	set_pic_params(info);
}

static GtkWidget *
create_slider (const char *name, gint value, GtkSignalFunc cb, gpointer data)
{
	GtkAdjustment *adj;

	GtkWidget *hbox;
	GtkWidget *slider;
	GtkWidget *label;

	hbox = gtk_hbox_new (FALSE, 5);

	label = gtk_label_new (name);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	adj = GTK_ADJUSTMENT (gtk_adjustment_new (value * 100. / 65535., 
						  0, 100., 1., 10., 10.));
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			    GTK_SIGNAL_FUNC (cb), data);

	slider = gtk_hscale_new (adj);
	gtk_box_pack_start (GTK_BOX (hbox), slider, TRUE, TRUE, 0);

	return hbox;
}

GtkWidget*
video_create_controls (VidServer_Server server)
{
	GtkWidget *slider;
	GtkWidget *vbox;
	char *params;
	GtkWidget *check_box;
	CORBA_Environment ev;
	VideoInfo *info = g_new (VideoInfo, 1);

	CORBA_exception_init(&ev);

	info->server = server;

	params = VidServer_Server_get_params (server, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_warning ("Could not fetch parameters\n");
	else
		sscanf(params, "%d %d %d %d", &info->brightness, &info->hue, &info->color, &info->contrast);
	/*  CORRBA_free (params); */ /* FIXME - how do we do this? */
	
	vbox = gtk_vbox_new (FALSE, 4);
  
	slider = create_slider ("Contrast: ", info->contrast, contrast_changed, info);
	gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);
	
	slider = create_slider ("Brightness: ", info->brightness, brightness_changed, info);
	gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);

	slider = create_slider ("Hue: ", info->brightness, hue_changed, info);
	gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);

	slider = create_slider ("Color: ", info->brightness, color_changed, info);
	gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);

	check_box = gtk_check_button_new_with_label ("Double Scan");
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (check_box), info->double_scan);
	gtk_box_pack_start (GTK_BOX (vbox), check_box, FALSE, FALSE, 0);

	gtk_signal_connect (GTK_OBJECT (check_box), "toggled",
			    GTK_SIGNAL_FUNC (double_changed), info);

	return vbox;
}
