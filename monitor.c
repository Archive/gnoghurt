/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "clientmain.h"
#include "vidserver.h"
#include <stdio.h>
#include <gdk/gdkx.h>
#include <X11/extensions/XShm.h>

typedef struct _FilterInfo FilterInfo;

char *paramstr = NULL;

struct _FilterInfo {
  VidChannel *channel;
  char *addr;
  long seqno;
};

static void frame_received (long seqno, char *addr, void *data);
static void channel_destroyed (void *data);

int frames;

int width, height, rowstride;
GtkWidget *darea;

CORBA_ORB orb;

GList *filters = NULL;

/* Render a 24-bit RGB image in buf into the GdkImage, without dithering.
   This assumes native byte ordering - what should really be done is to
   check whether static_image->byte_order is consistent with the _ENDIAN
   config flag, and if not, use a different function.

   This one is even faster than the one below - its inner loop loads 3
   words (i.e. 4 24-bit pixels), does a lot of shifting and masking,
   then writes 2 words. */
static void
rgb_convert_565 (guchar *buf, guchar *obuf)
{
  int x, y;
  guchar *obptr;
  gint bpl;
  guchar *bptr, *bp2;
  guchar r, g, b;

  bptr = buf;
  bpl = width*2;
  for (y = 0; y < height; y++)
    {
      bp2 = bptr;
      obptr = obuf;
      if (((unsigned long)obuf | (unsigned long) bp2) & 3)
	{
	  for (x = 0; x < width; x++)
	    {
	      b = *bp2++;
	      g = *bp2++;
	      r = *bp2++;
	      ((guint16 *)obptr)[0] = ((r & 0xf8) << 8) |
		((g & 0xfc) << 3) |
		(b >> 3);
	      obptr += 2;
	    }
	}
      else
	{
	  for (x = 0; x < width - 3; x += 4)
	    {
	      guint32 b1r0g0b0;
	      guint32 g2b2r1g1;
	      guint32 r3g3b3r2;

	      b1r0g0b0 = ((guint32 *)bp2)[0];
	      g2b2r1g1 = ((guint32 *)bp2)[1];
	      r3g3b3r2 = ((guint32 *)bp2)[2];
	      ((guint32 *)obptr)[0] =
		((b1r0g0b0 & 0xf8) >> 3) |        /* 3  - 0  */
		((b1r0g0b0 & 0xfc00) >> 5) |      /* 10 - 5 */
		((b1r0g0b0 & 0xf80000) >> 8) |    /* 19 - 11 */
		((b1r0g0b0 & 0xf8000000) >> 11) | /* 27 - 16 */
		((g2b2r1g1 & 0xfc) << 19) |       /* 2  - 21 */
		((g2b2r1g1 & 0xf800) << 16);      /* 11 - 27 */
	      ((guint32 *)obptr)[1] =
		((g2b2r1g1 & 0xf80000) >> 19) |   /* 19 - 0 */
		((g2b2r1g1 & 0xfc000000) >> 21) | /* 26 - 5 */
		((r3g3b3r2 & 0xf8)  << 8) |       /* 3 - 11 */
		((r3g3b3r2 & 0xf800) << 5) |      /* 11 - 16 */
		((r3g3b3r2 & 0xfc0000) << 3) |    /* 18 - 21 */
		(r3g3b3r2 & 0xf8000000);          /* 27 - 27 */
	      bp2 += 12;
	      obptr += 8;
	    }
	  for (; x < width; x++)
	    {
	      r = *bp2++;
	      g = *bp2++;
	      b = *bp2++;
	      ((guint16 *)obptr)[0] = ((r & 0xf8) << 8) |
		((g & 0xfc) << 3) |
		(b >> 3);
	      obptr += 2;
	    }
	}
      bptr += rowstride;
      obuf += bpl;
    }
}

static GdkImage *out_image = NULL;

void create_out_image ()
{
  static GdkPixmap *bg_pixmap = NULL;

  if (GTK_WIDGET_REALIZED (darea))
    {
      XShmSegmentInfo *x_shm_info;
      Pixmap out_pmap;
      
      if (out_image)
	{
	  gdk_pixmap_unref (bg_pixmap);
	  gdk_flush();
	  gdk_image_destroy (out_image);
	}
      
      out_image = gdk_image_new (GDK_IMAGE_FASTEST, gdk_rgb_get_visual(),
				 width, height);
      
      x_shm_info = ((GdkImagePrivate *)out_image)->x_shm_info;
      
      out_pmap = XShmCreatePixmap (GDK_DISPLAY(), GDK_ROOT_WINDOW(),
				   out_image->mem,
				   x_shm_info,
				   width, height,
				   gdk_visual_get_system()->depth);
      
      bg_pixmap = gdk_pixmap_foreign_new (out_pmap);
      gdk_window_set_back_pixmap (darea->window, bg_pixmap, FALSE);
    }
}

gboolean
draw_frame (gpointer data)
{
  FilterInfo *info = data;

  if (GTK_WIDGET_REALIZED (darea))
    {
      if (!out_image)
	{
	  create_out_image();
	}

      gdk_flush();
      rgb_convert_565 (info->addr, out_image->mem);
      gdk_window_clear (darea->window);
    }
  
  vid_channel_frame_done (info->channel);

  return FALSE;
}

static void 
frame_received (long seqno, char *addr, void *data)
{
  FilterInfo *info = data;
  static long last_seqno = -1;

  if (last_seqno != -1 && seqno < last_seqno)
    {
      /* Just discard out-of-order frames for now */
      vid_channel_frame_done (info->channel);
      return;
    }

  frames++;

  last_seqno = seqno;

  info->addr = addr;
  info->seqno = seqno;
  gtk_idle_add (draw_frame, info);
}

static void 
channel_destroyed (void *data)
{
  FilterInfo *info = data;
  
  filters = g_list_remove (filters, info);
  g_free (info);
}

static gboolean
fps_timeout (gpointer data)
{
  GtkWidget *label = data;
  char *text;

  text = g_strdup_printf ("%d", frames);
  gtk_label_set_text (GTK_LABEL (label), text);
  g_free (text);

  frames = 0;

  return TRUE;
}

static void
create_gui (void)
{
  GtkWidget *window;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *label;
  
  gdk_rgb_init();

  gtk_widget_push_visual (gdk_rgb_get_visual());
  gtk_widget_push_colormap (gdk_rgb_get_cmap());

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
  
  label = gtk_label_new ("0");
  gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
  gtk_timeout_add (1000, fps_timeout, label);
  
  label = gtk_label_new ("Frames / second: ");
  gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  darea = gtk_drawing_area_new ();
  gtk_widget_set_usize (darea, width, height);

  gtk_box_pack_start (GTK_BOX (vbox), darea, FALSE, FALSE, 0);

  gtk_widget_show_all (window);
}

void add_source (VidServer_Channel src, void *data)
{
  gint w, h, r;
  FilterInfo *info = g_new (FilterInfo, 1);

  info->channel = vid_channel_open (orb, src, &w, &h, &r, frame_received, channel_destroyed, info);

  if (filters)
    {
      if (w != width || h != height || r != rowstride)
	{
	  g_warning ("Mixed size sources!\n");
	  vid_channel_close (info->channel);
	  g_free (info);
	  return;
	}
    }
  else
    {
      width = w;
      height = h;
      rowstride = r;

      gtk_widget_set_usize (darea, width, height);

      create_out_image();
    }

  filters = g_list_prepend (filters, info);
}

static void 
server_destroy (void *data)
{
  gtk_main_quit();
}

int 
main (int argc, char **argv)
{
  CORBA_Environment ev;
  GList *tmp_list;
  VidServer *server;

  VidServer_Server servobj;
  char *ior;

  orb = vid_init (&argc, &argv);
  gtk_init (&argc, &argv);

  CORBA_exception_init(&ev);
  
  width = 640;
  height = 480;
  rowstride = width*3;

  server = vid_server_create (orb, width, height, rowstride,
			      NULL, NULL, add_source, server_destroy, NULL);

  servobj = vid_server_get_obj (server);

  ior = CORBA_ORB_object_to_string (orb, servobj, &ev);
  g_print ("%s\n", ior);
  CORBA_free (ior);

  CORBA_Object_release (servobj, &ev);

  create_gui();
  gtk_main ();

  tmp_list = filters;
  while (tmp_list)
    {
      FilterInfo *filter = tmp_list->data;
      tmp_list = tmp_list->next;

      vid_channel_close (filter->channel);
    }

  return 0;
}


