/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <glib.h>
#include <sys/shm.h>

#include "vidserver.h"

struct _VidChannel
{
  POA_VidServer_ChannelClient servant;

  VidServer_Channel channel;

  VidServerFrameFunc frame_cb;
  VidServerDestroyFunc destroy_cb;
  void *cb_data;

  VidServer_Key key;
  CORBA_long seqno;
  char *addr;

  CORBA_ORB orb;
  PortableServer_POA poa;
};

/* Forward declarations */

static void  client_frame_received (PortableServer_Servant servant, 
				    const CORBA_long seqno, 
				    const CORBA_long shmid,
				    CORBA_Environment * ev);
static void  client_destroy        (PortableServer_Servant servant, 
				    CORBA_Environment * ev);

/* Servant declarations */

static PortableServer_ServantBase__epv base_epv = {
  NULL,
  NULL,
  NULL
};

static POA_VidServer_ChannelClient__epv client_epv = {
  NULL, client_frame_received, client_destroy
};

static POA_VidServer_ChannelClient__vepv client_vepv = {
  &base_epv, &client_epv
};

/* Public Functions */

VidChannel *
vid_channel_open (CORBA_ORB orb, VidServer_Channel ch,
		  int *width, int *height, int *rowstride,
		  VidServerFrameFunc frame_cb,
		  VidServerDestroyFunc destroy_func,
		  void *data)
{
  VidChannel *channel = g_new (VidChannel, 1);
  VidServer_ChannelClient client;
  CORBA_Environment ev;
  CORBA_long twidth, theight, trowstride;

  CORBA_exception_init(&ev);

  channel->channel = CORBA_Object_duplicate (ch, &ev);

  channel->frame_cb = frame_cb;
  channel->destroy_cb = destroy_func;
  channel->cb_data = data;

  channel->addr = NULL;

  channel->orb = orb;
  channel->poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);

  channel->servant._private = NULL;
  channel->servant.vepv = &client_vepv;

  POA_VidServer_ChannelClient__init (&channel->servant, &ev);
  PortableServer_POA_activate_object (channel->poa, &channel->servant, &ev);
  client = PortableServer_POA_servant_to_reference (channel->poa, &channel->servant, &ev);

  channel->key = VidServer_Channel_open (ch, client, 
					 &twidth, &theight, &trowstride, &ev);

  if (height)
    *height = theight;
  if (width)
    *width = twidth;
  if (rowstride)
    *rowstride = trowstride;

  CORBA_Object_release (client, &ev);

  return channel;
}

void
vid_channel_frame_done (VidChannel *channel)
{
  CORBA_Environment ev;

  g_return_if_fail (channel->addr != NULL);
  g_return_if_fail (channel->channel != NULL);

  CORBA_exception_init(&ev);

  shmdt (channel->addr);
  channel->addr = NULL;

  VidServer_Channel_frame_done (channel->channel, channel->key, channel->seqno, &ev);
}


void
vid_channel_close (VidChannel *channel)
{
  CORBA_Environment ev;
  PortableServer_POA poa;
  PortableServer_ObjectId *objid;

  CORBA_exception_init(&ev);

  if (channel->addr)
    channel->addr = NULL;

  if (channel->channel)
    {
      VidServer_Channel_close (channel->channel, channel->key, &ev);
      CORBA_Object_release (channel->channel, &ev);
      channel->channel = NULL;
    }

  poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(channel->orb, "RootPOA", &ev);

  objid = PortableServer_POA_servant_to_id (poa, &channel->servant, &ev);
  PortableServer_POA_deactivate_object (poa, objid, &ev);
  CORBA_free (objid);
}

/* Callback Functions */

static void 
client_frame_received (PortableServer_Servant servant, 
		       const CORBA_long seqno, 
		       const CORBA_long shmid,
		       CORBA_Environment * ev)
{
  VidChannel *channel = (VidChannel *)servant;
  char *addr;

  channel->seqno = seqno;

  addr = shmat (shmid, 0, 0);
  if (addr != (char *)-1)
    {
      channel->addr = addr;
      channel->frame_cb (seqno, addr, channel->cb_data);
    }
}
 
static void
client_destroy (PortableServer_Servant servant, CORBA_Environment * ev)
{
  VidChannel *channel = (VidChannel *)servant;

  if (channel->addr)
    shmdt (channel->addr);

  if (channel->channel)
    {
      CORBA_Object_release (channel->channel, ev);
      channel->channel = NULL;
    }

  channel->destroy_cb (channel->cb_data);
}

