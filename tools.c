/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gnome.h>
#include "filter-edit.h"
#include "filters.h"
#include "tools.h"


typedef struct {
	double dx, dy;
} MoveData;

typedef struct {
	FilterEdit *fe;
	Link *link;
	guint motion_handler_id;
	guint button_handler_id;
} LinkData;

gint
move_tool_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	FilterEdit *fe;
	Filter *f;
	GtkArg args[2];
	MoveData *md;

	fe = FILTER_EDIT (data);

	f = gtk_object_get_data (GTK_OBJECT (item), "filter");
	if (!f)
		return FALSE; /* Cannot move non-filter objects */

	md = fe->tool_data;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button != 1)
			break;

		fe->tool_data = md = g_new (MoveData, 1);

		args[0].name = "x";
		args[1].name = "y";
		gtk_object_getv (GTK_OBJECT (item), 2, args);

		md->dx = GTK_VALUE_DOUBLE (args[0]) - event->button.x;
		md->dy = GTK_VALUE_DOUBLE (args[1]) - event->button.y;

		gnome_canvas_item_grab (item,
					GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
					NULL,
					event->button.time);
		return TRUE;

	case GDK_BUTTON_RELEASE:
		if (!(md && event->button.button == 1))
			break;

		filter_move_to (f, event->button.x + md->dx, event->button.y + md->dy);
		gnome_canvas_item_ungrab (item, event->button.time);
		g_free (md);
		fe->tool_data = NULL;
		return TRUE;

	case GDK_MOTION_NOTIFY:
		if (!(md && (event->button.state & GDK_BUTTON1_MASK)))
			break;

		filter_move_to (f, event->button.x + md->dx, event->button.y + md->dy);
		return TRUE;

	default:
		break;
	}

	return FALSE;
}

static gint
link_motion (GtkWidget *widget, GdkEventMotion *event, gpointer data)
{
	FilterEdit *fe;
	LinkData *ld;
	GtkArg arg;
	GnomeCanvasPoints *points;
	double x, y;

	fe = FILTER_EDIT (data);
	ld = fe->tool_data;

	arg.name = "points";
	gtk_object_getv (GTK_OBJECT (ld->link->item), 1, &arg);
	points = GTK_VALUE_BOXED (arg);

	gnome_canvas_window_to_world (GNOME_CANVAS (fe->canvas), event->x, event->y, &x, &y);
	points->coords[2] = x;
	points->coords[3] = y;

	gnome_canvas_item_set (ld->link->item,
			       "points", points,
			       NULL);

	gnome_canvas_points_free (points);

	return TRUE;
}

static gint
link_button (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	FilterEdit *fe;
	LinkData *ld;

	fe = FILTER_EDIT (data);
	ld = fe->tool_data;

	gtk_signal_disconnect (GTK_OBJECT (fe->canvas), ld->motion_handler_id);
	gtk_signal_disconnect (GTK_OBJECT (fe->canvas), ld->button_handler_id);

	gtk_object_destroy (GTK_OBJECT (ld->link->item));
	g_free (ld->link);

	g_free (ld);
	fe->tool_data = NULL;

	return TRUE;
}

static void
finalize_link (Link *link, Filter *dest)
{
	GtkArg args[2];
	GnomeCanvasPoints *points;

	args[0].name = "points";
	gtk_object_getv (GTK_OBJECT (link->item), 1, args);
	points = GTK_VALUE_BOXED (args[0]);

	args[0].name = "x";
	args[1].name = "y";
	gtk_object_getv (GTK_OBJECT (dest->item), 2, args);
	points->coords[2] = GTK_VALUE_DOUBLE (args[0]);
	points->coords[3] = GTK_VALUE_DOUBLE (args[1]);
	gnome_canvas_item_set (link->item,
			       "points", points,
#if 0
			       "last_arrowhead", FALSE,
#endif
			       NULL);

	gnome_canvas_points_free (points);

	link->dest = dest;

	filter_activate_link (link);
}

static Link *
create_link_from (GnomeCanvasGroup *group, Filter *f)
{
	Link *link;
	GnomeCanvasPoints *points;
	GtkArg args[2];

	link = g_new0 (Link, 1);

	args[0].name = "x";
	args[1].name = "y";
	gtk_object_getv (GTK_OBJECT (f->item), 2, args);

	points = gnome_canvas_points_new (2);
	points->coords[0] = GTK_VALUE_DOUBLE (args[0]);
	points->coords[1] = GTK_VALUE_DOUBLE (args[1]);
	points->coords[2] = points->coords[0];
	points->coords[3] = points->coords[1];

	link->item = gnome_canvas_item_new (group,
					    gnome_canvas_line_get_type (),
					    "points", points,
					    "fill_color", "midnightblue",
#if 0
					    "last_arrowhead", TRUE,
					    "arrow_shape_a", 8.0,
					    "arrow_shape_b", 12.0,
					    "arrow_shape_c", 4.0,
#endif
					    NULL);
	gnome_canvas_points_free (points);

	gtk_object_set_data (GTK_OBJECT (link->item), "link", link);

	link->source = f;

	return link;
}

gint
link_tool_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	FilterEdit *fe;
	Filter *f;
	LinkData *ld;

	fe = FILTER_EDIT (data);

	f = gtk_object_get_data (GTK_OBJECT (item), "filter");
	if (!f)
		return FALSE; /* Cannot link non-filter objects */

	ld = fe->tool_data;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (event->button.button != 1)
			break;

		if (!ld) {
			/* Source part */

			fe->tool_data = ld = g_new (LinkData, 1);
			ld->fe = fe;
			ld->link = create_link_from (fe->link_group, f);

			ld->motion_handler_id = gtk_signal_connect (
				GTK_OBJECT (fe->canvas), "motion_notify_event",
				GTK_SIGNAL_FUNC (link_motion),
				fe);
			ld->button_handler_id = gtk_signal_connect_after (
				GTK_OBJECT (fe->canvas), "button_press_event",
				GTK_SIGNAL_FUNC (link_button),
				fe);
		} else {
			/* Destination part */

			if (ld->fe != fe || f == ld->link->source || f->parent_link != NULL)
				break;

			finalize_link (ld->link, f);

			gtk_signal_disconnect (GTK_OBJECT (fe->canvas), ld->motion_handler_id);
			gtk_signal_disconnect (GTK_OBJECT (fe->canvas), ld->button_handler_id);

			g_free (ld);
			fe->tool_data = NULL;
		}

		gtk_signal_emit_stop_by_name (GTK_OBJECT (fe->canvas), "button_press_event");
		return TRUE;

	default:
		break;
	}

	return FALSE;
}

/* Tries to delete a filter, and returns TRUE on success */
static gboolean
delete_filter (GnomeCanvasItem *item)
{
	Filter *f;

	f = gtk_object_get_data (GTK_OBJECT (item), "filter");
	if (!f)
		return FALSE;

	filter_destroy (f);
	return TRUE;
}

static gboolean
delete_link (GnomeCanvasItem *item)
{
	Link *link;

	link = gtk_object_get_data (GTK_OBJECT (item), "link");
	if (!link)
		return FALSE;

	/* FIXME */
}

gint
delete_tool_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	Filter *f;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (!delete_filter (item) || !delete_link (item))
			break;
		else
			return TRUE;

		break;

	default:
		break;
	}

	return FALSE;
}
