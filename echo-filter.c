/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <glib.h>
#include <string.h>
#include <stdlib.h>
#include "filtermain.h"

int width, height, rowstride;
guint32 *histbuf;
int framecount = 0;
int trails = 1;

void 
set_size (int w, int h, int r)
{
  width = w;
  height = h;
  rowstride = r;
  histbuf = (guint32 *) malloc(height * rowstride * 8);
}

void
set_params (const char *str)
{
	trails = atoi(str);
}


char *
get_params (void)
{
	return g_strdup_printf ("%d", trails);
}



void 
process_frame (void *in_addr, void *out_addr)
{
  int i, m, n, offset;
  guint32 *src, *dst, *hist;

  src = in_addr;
  dst = out_addr;
  offset = height * (rowstride / 4);
  n = (framecount & 7);
  memcpy(dst, src, offset * 4);
  if (trails > 0) {
	for (m = 1; m <= trails; m++) {	 /* newest has most weight */
		hist = histbuf + offset * ((m + n - trails) & 7);
		dst = out_addr;
		for (i = 0; i < offset; i++) {
			*dst = ((*dst++ & 0xfefefefe)>>1)
				+ ((*hist++ & 0xfefefefe)>>1);
		}
		
	}
  }
  memcpy(histbuf + offset * n, out_addr, offset * 4);
  framecount++;
}
/* eof echo-filter.c */
