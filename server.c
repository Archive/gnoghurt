/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <glib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <errno.h>
#include <stdio.h>

#include "vidserver.h"

typedef struct _VidServerChannel VidServerChannel;
typedef struct _VidServerClient VidServerClient;
typedef struct _VidServerFrame VidServerFrame;

struct _VidServer 
{
  POA_VidServer_Server servant;

  CORBA_long width;
  CORBA_long height;
  CORBA_long rowstride;

  CORBA_ORB orb;
  PortableServer_POA poa;
  
  GList *channels;
  GList *free_frames;

  VidServerFrame *current_frame;
  VidServerFrame *next_frame;

  VidServerAddSourceFunc add_source_func;
  VidServerGetParamsFunc get_params_func;
  VidServerSetParamsFunc set_params_func;
  VidServerDestroyFunc destroy_func;
  void *cb_data;

  CORBA_long key;

  guint refcount;
};

struct _VidServerFrame
{
  int shmid;
  char *addr;
  int refcount;
  CORBA_long seqno;
};

struct _VidServerClient
{
  VidServer_ChannelClient client;

  VidServer_Key key;
  VidServerFrame *frame;

  gboolean valid:1;

  guint refcount;
};

struct _VidServerChannel 
{
  POA_VidServer_Channel servant;

  VidServer *server;
  GList *clients;
  CORBA_long seqno;

  guint refcount;
};

/* Forward Declarations */

static VidServer_Channel server_create_channel (PortableServer_Servant servant,
						CORBA_Environment    *ev);
static void server_add_source                  (PortableServer_Servant servant,
						const VidServer_Channel src,
						CORBA_Environment * ev);
static void server_set_params                  (PortableServer_Servant servant,
						const CORBA_char * params, 
						CORBA_Environment * ev);
static char *server_get_params                 (PortableServer_Servant servant,
						CORBA_Environment * ev);
static void server_destroy                     (PortableServer_Servant servant,
						CORBA_Environment * ev);
static VidServer_Key      channel_open (PortableServer_Servant servant, 
				      const VidServer_ChannelClient client_obj,
				      CORBA_long *width, 
				      CORBA_long *height, 
				      CORBA_long *rowstride,
				      CORBA_Environment * ev);
void channel_close (PortableServer_Servant servant, 
		    const VidServer_Key key, 
		    CORBA_Environment *ev);
void channel_frame_done (PortableServer_Servant servant, 
			 const VidServer_Key k, 
			 const CORBA_long seqno,
			  CORBA_Environment * ev);
void channel_destroy (PortableServer_Servant servant, 
		      CORBA_Environment * ev);

static void channel_unref (VidServerChannel *channel);
static void channel_ref (VidServerChannel *channel);

static void  server_unref_frame  (VidServer        *server,
				  VidServerFrame   *frame);
static void  channel_fill        (VidServerChannel *channel);
static void  channel_free_client (VidServerChannel *channel, 
				  VidServerClient  *client);
static void  channel_unref_client (VidServerChannel *channel, 
				   VidServerClient *client);

/* Servant vtable initialization */

static PortableServer_ServantBase__epv base_epv = { NULL };
static POA_VidServer_Server__epv server_epv = { NULL };

static POA_VidServer_Server__vepv server_vepv = {
  &base_epv, &server_epv
};

static POA_VidServer_Channel__epv channel_epv = {
  NULL, channel_open, channel_close, channel_frame_done, channel_destroy
};

static POA_VidServer_Channel__vepv channel_vepv = {
  &base_epv, &channel_epv
};

VidServer *
vid_server_create (CORBA_ORB orb,
		   int width, int height, int rowstride,
		   VidServerGetParamsFunc get_params_func,
		   VidServerSetParamsFunc set_params_func,
		   VidServerAddSourceFunc add_source_func,
		   VidServerDestroyFunc destroy_func,
		   char *data)
{
  static gboolean initialized = FALSE;
  VidServer *server;
  CORBA_Environment ev;
  CORBA_exception_init(&ev);

  if (!initialized)
    {
      server_epv.create_channel = server_create_channel;
      server_epv.add_source = server_add_source;
      server_epv.set_params = server_set_params;
      server_epv.get_params = server_get_params;
      server_epv.destroy = server_destroy;
      initialized = TRUE;
    }
  
  server = g_new (VidServer, 1);
  server->refcount = 1;
  
  server->width = width;
  server->height = height;
  server->rowstride = rowstride;
  
  server->channels = NULL;
  server->free_frames = NULL;

  server->current_frame = NULL;
  server->next_frame = NULL;

  server->key = 0;

  server->orb = orb;
  server->poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);

  server->servant._private = NULL;
  server->servant.vepv = &server_vepv;

  server->add_source_func = add_source_func;
  server->get_params_func = get_params_func;
  server->set_params_func = set_params_func;
  server->destroy_func = destroy_func;
  server->cb_data = data;

  POA_VidServer_Server__init (&server->servant, &ev);
  PortableServer_POA_activate_object (server->poa, &server->servant, &ev);

  return server;
}

void
vid_server_set_size (VidServer *server, int width, int height, int rowstride)
{
  g_return_if_fail (server->channels == NULL);

  server->width = width;
  server->height = height;
  server->rowstride = rowstride;
}

VidServer_Server
vid_server_get_obj (VidServer *server)
{
  CORBA_Environment ev;
  CORBA_exception_init(&ev);

  return PortableServer_POA_servant_to_reference (server->poa, &server->servant, &ev);
}

char *
vid_server_get_frame (VidServer *server, long seqno)
{
  g_return_val_if_fail (server->next_frame == NULL, server->next_frame->addr);

  if (server->free_frames)
    {
      GList *tmp_node;
      server->next_frame = server->free_frames->data;

      tmp_node = server->free_frames;
      server->free_frames = g_list_remove_link (server->free_frames, server->free_frames);
      g_list_free_1 (tmp_node);
    }
  else
    {
      server->next_frame = g_new (VidServerFrame, 1);
      server->next_frame->shmid = shmget (IPC_PRIVATE, 
					  server->height * server->rowstride,
					  IPC_CREAT | 0777);
      server->next_frame->addr = shmat (server->next_frame->shmid, 0, 0);
      
      /* Fixme, this won't work on all systems; some systems
       * don't allow attaching to a destroyed segment
       */
      shmctl (server->next_frame->shmid, IPC_RMID, 0);
    }
    
  server->next_frame->seqno = seqno;
  server->next_frame->refcount = 1;

  return server->next_frame->addr;
}

void 
vid_server_write_frame (VidServer *server)
{
  GList *tmp_list;
  
  g_return_if_fail (server->next_frame != NULL);
  
  /* If we are still trying to pump out the current frame at
   * this point, give up
   */
  if (server->current_frame)
    server_unref_frame (server, server->current_frame);

  server->current_frame = server->next_frame;
  server->next_frame = NULL;

  server->refcount++;
  tmp_list = server->channels;
  g_list_foreach (server->channels, (GFunc)channel_ref, NULL);
  g_list_foreach (server->channels, (GFunc)channel_fill, NULL);
  g_list_foreach (server->channels, (GFunc)channel_unref, NULL);
  vid_server_unref (server);
}

void
vid_server_unref (VidServer *server)
{
  GList *tmp_list;
  PortableServer_ObjectId *objid;

  server->refcount--;
  if (server->refcount == 0)
    {
      CORBA_Environment ev;
      CORBA_exception_init(&ev);

      tmp_list = server->channels;
      while (tmp_list)
	{
	  VidServerChannel *channel = tmp_list->data;
	  tmp_list = tmp_list->next;
	  
	  channel_unref (channel);
	}
      
      tmp_list = server->free_frames;
      while (tmp_list)
	{
	  VidServerFrame *frame = tmp_list->data;
	  shmdt (frame->addr);
	  g_free (frame);
	  
	  tmp_list = tmp_list->next;
	}
      
      objid = PortableServer_POA_servant_to_id (server->poa, &server->servant, &ev);
      PortableServer_POA_deactivate_object (server->poa, objid, &ev);
      CORBA_free (objid);
      
      g_list_free (server->free_frames);
      g_free (server);
    }
}

static VidServer_Channel
server_create_channel (PortableServer_Servant servant,
		       CORBA_Environment    *ev)
{
  VidServer *server = (VidServer *)servant;
  VidServerChannel *channel;

  channel = g_new (VidServerChannel, 1);
  channel->refcount = 1;

  server->channels = g_list_prepend (server->channels, channel);

  channel->clients = NULL;
  channel->seqno = 0;
  channel->server = server;

  channel->servant._private = NULL;
  channel->servant.vepv = &channel_vepv;

  POA_VidServer_Channel__init (&channel->servant, ev);
  PortableServer_POA_activate_object (server->poa, &channel->servant, ev);

  return PortableServer_POA_servant_to_reference (server->poa, &channel->servant, ev);
}

static void 
server_add_source (PortableServer_Servant servant,
		   const VidServer_Channel src,
		   CORBA_Environment * ev)
{
  VidServer *server = (VidServer *)servant;

  if (server->add_source_func)
    server->add_source_func (src, server->cb_data);
}

static void 
server_set_params (PortableServer_Servant servant,
		   const CORBA_char * params, 
		   CORBA_Environment * ev)
{
  VidServer *server = (VidServer *)servant;

  if (server->set_params_func)
    server->set_params_func (params, server->cb_data);
}

static char *
server_get_params (PortableServer_Servant servant,
		   CORBA_Environment * ev)
{
  VidServer *server = (VidServer *)servant;

  if (server->get_params_func)
    return server->get_params_func (server->cb_data);

  else
    return CORBA_string_dup ("");
}

static void
server_destroy (PortableServer_Servant servant,
		   CORBA_Environment * ev)
{
  VidServer *server = (VidServer *)servant;

  if (server->destroy_func)
    return server->destroy_func (server->cb_data);
}

static VidServer_Key
channel_open (PortableServer_Servant servant, 
	      const VidServer_ChannelClient client_obj,
	      CORBA_long *width, CORBA_long *height, CORBA_long *rowstride,
	      CORBA_Environment * ev)
{
  VidServerChannel *channel = (VidServerChannel *)servant;
  VidServerClient *client = g_new (VidServerClient, 1);
  client->refcount = 1;

  channel->clients = g_list_prepend (channel->clients, client);

  client->client = CORBA_Object_duplicate(client_obj, ev);
  client->key = ++channel->server->key;
  client->frame = NULL;

  *width = channel->server->width;
  *height = channel->server->height;
  *rowstride = channel->server->rowstride;

  return client->key;
}

void 
channel_close (PortableServer_Servant servant, 
	       VidServer_Key key, 
	       CORBA_Environment *ev)
{
  GList *tmp_list;
  VidServerChannel *channel = (VidServerChannel *)servant;
  VidServerClient *client;

  tmp_list = channel->clients;
  while (tmp_list)
    {
      client = tmp_list->data;
      if (client->key == key)
	break;
      
      tmp_list = tmp_list->next;
    }

  if (!tmp_list)		/* XXX: Throw an exception */
    return;

  channel_unref_client (channel, client);
}

void 
channel_frame_done (PortableServer_Servant servant,
		    VidServer_Key key,
		    const CORBA_long seqno, 
		    CORBA_Environment * ev)
{
  GList *tmp_list;
  VidServerChannel *channel = (VidServerChannel *)servant;
  VidServerClient *client;

  tmp_list = channel->clients;
  while (tmp_list)
    {
      client = tmp_list->data;

      if (client->key == key)
	break;

      tmp_list = tmp_list->next;
    }

  if (!tmp_list)		/* XXX: Throw an exception */
    return;

  if (client->frame)
    {
      server_unref_frame (channel->server, client->frame);
      client->frame = NULL;
    }

  channel_fill (channel);
}

void 
channel_destroy (PortableServer_Servant servant, 
		 CORBA_Environment * ev)
{
  VidServerChannel *channel = (VidServerChannel *)servant;

  channel_unref (channel);
}

/* Utility functions */
static void 
server_unref_frame (VidServer *server,
		    VidServerFrame *frame)
{
  frame->refcount --;
  if (frame->refcount == 0)
    server->free_frames = g_list_prepend (server->free_frames, frame);
}

static void
channel_fill (VidServerChannel *channel)
{
  if (channel->seqno != channel->server->current_frame->seqno)
    {
      CORBA_Environment ev;
      GList *tmp_list = channel->clients;

      CORBA_exception_init(&ev);

      while (tmp_list)
	{
	  VidServerClient *client = tmp_list->data;
	  tmp_list = tmp_list->next;

	  if (!client->frame)
	    {
	      client->frame = channel->server->current_frame;
	      client->frame->refcount++;
	      channel->seqno = channel->server->current_frame->seqno;

	      VidServer_ChannelClient_frame_received (client->client,
						      client->frame->seqno,
						      client->frame->shmid, &ev);

	      return;
	    }
	}
    }
}

static void
channel_ref (VidServerChannel *channel)
{
  channel->refcount++;
}

static void
channel_unref (VidServerChannel *channel)
{
  channel->refcount--;

  if (channel->refcount == 0)
    {
      GList *tmp_list;
      PortableServer_ObjectId *objid;

      CORBA_Environment ev;
      CORBA_exception_init(&ev);

      channel->server->channels = g_list_remove (channel->server->channels, channel);
      
      tmp_list = channel->clients;
      while (tmp_list)
	{
	  VidServerClient *client = tmp_list->data;
	  tmp_list = tmp_list->next;
	  
	  channel_unref_client (channel, client);
	}
      
      objid = PortableServer_POA_servant_to_id (channel->server->poa, &channel->servant, &ev);
      PortableServer_POA_deactivate_object (channel->server->poa, objid, &ev);
      CORBA_free (objid);

      g_free (channel);
    }
}

static void
channel_unref_client (VidServerChannel *channel, 
		     VidServerClient *client)
{
  CORBA_Environment ev;
  CORBA_exception_init(&ev);

  client->refcount--;

  if (client->refcount == 0)
    {
      channel->clients = g_list_remove (channel->clients, client);
  
      if (client->frame)
	server_unref_frame (channel->server, client->frame);

      CORBA_Object_release (client->client, &ev);
      g_free (client);
    }
}

void   
vid_server_write_ior (VidServer *server)
{
  CORBA_Environment ev;
  char *filename;
  char *ior;
  VidServer_Server servobj;
  int fd;
  
  servobj = vid_server_get_obj (server);

  filename = g_strconcat (g_get_home_dir(), "/", ".vidserver.ior", NULL);
  fd = open (filename, O_CREAT | O_WRONLY, 0644);
  if (fd < 0)
    g_error ("Could not open '%s': %s", filename, g_strerror (errno));

  CORBA_exception_init(&ev);

  ior = CORBA_ORB_object_to_string (server->orb, servobj, &ev);
  write (fd, ior, strlen(ior));
  write (fd, "\n", 1);

  close (fd);
  
  CORBA_free (ior);
  g_free (filename);
}
