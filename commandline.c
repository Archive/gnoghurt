/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <errno.h>
#include <ctype.h>
#include "vidserver.h"

typedef struct _ServerInfo ServerInfo;

struct _ServerInfo {
  VidServer_Server server;
  VidServer_Channel in_channel;
  int key;
};

int curkey = 0;

GList *servers;

int 
main (int argc, char **argv)
{
  CORBA_Environment ev;
  CORBA_ORB  orb;
  GList *tmp_list;

  ServerInfo *info;
  char buffer[1024];
  int filt1, filt2;
  
  orb = vid_init (&argc, &argv);

  CORBA_exception_init(&ev);

  info = g_new (ServerInfo, 1);
  info->server = vid_server_ior_read (orb);
  info->in_channel = NULL;
  servers = g_list_prepend (servers, info);
  printf ("  Video stream started as: %d\n", info->key);

  while (1)
    {
      char *p;
      
      printf ("> ");
      fflush(stdout);
      fgets(buffer, 1024, stdin);

      p = strchr (buffer, '\n');
      if (p)
	*p = '\0';

      if (strncmp(buffer, "quit", 4) == 0)
	break;
      else if (strncmp(buffer, "spawn", 5) == 0)
	{
	  p = buffer+5;
	  while (isspace(*p))
	    p++;
	  
	  info = g_new (ServerInfo, 1);
	  info->in_channel = NULL;
	  info->server = vid_run_server (orb, p);

	  if (info->server)
	    {
	      info->key = ++curkey;
	      printf ("  started as: %d\n", info->key);
	      servers = g_list_prepend (servers, info);
	    }
	  else
	    g_print ("Spawn of '%s' failed\n", p);
	}
      else if (sscanf(buffer, "connect %d %d", &filt1, &filt2) == 2)
	{
	  ServerInfo *filter1 = NULL;
	  ServerInfo *filter2 = NULL;
	  
	  tmp_list = servers;
	  while (tmp_list)
	    {
	      ServerInfo *info = tmp_list->data;
	      if (filt1 == info->key)
		filter1 = info;
	      if (filt2 == info->key)
		filter2 = info;

	      tmp_list = tmp_list->next;
	    }

	  if (!filter1)
	    g_print ("Filter %d not found", filt1);
	  else if (!filter1)
	    g_print ("Filter %d not found", filt1);
	  else
	    {
	      if (filter2->in_channel)
		{
		  g_print ("Filter %d already connected\nmake", filt2);
		}
	      else
		{
		  filter2->in_channel = VidServer_Server_create_channel (filter1->server, &ev);
		  if (ev._major != CORBA_NO_EXCEPTION)
		    g_error ("Could not create channel\n");

		  VidServer_Server_add_source (filter2->server, filter2->in_channel, &ev);
		  if (ev._major != CORBA_NO_EXCEPTION)
		    g_error ("Could not connect filters\n");
		}
	    }
	}
      else if (sscanf(buffer, "disconnect %d", &filt1) == 1)
	{
	  ServerInfo *filter1 = NULL;
	  
	  tmp_list = servers;
	  while (tmp_list)
	    {
	      info = tmp_list->data;
	      if (filt1 == info->key)
		filter1 = info;

	      tmp_list = tmp_list->next;
	    }

	  if (!filter1)
	    g_print ("Filter %d not found", filt1);
	  else
	    {
	      if (filter1->in_channel)
		{
		  VidServer_Channel_destroy (filter1->in_channel, &ev);
		  filter1->in_channel = NULL;
		}
	    }
	}
      else if (sscanf(buffer, "kill %d", &filt1) == 1)
	{
	  ServerInfo *filter1 = NULL;
	  
	  tmp_list = servers;
	  while (tmp_list)
	    {
	      info = tmp_list->data;
	      if (filt1 == info->key)
		filter1 = info;

	      tmp_list = tmp_list->next;
	    }

	  if (!filter1)
	    g_print ("Filter %d not found", filt1);
	  else
	    {
	      VidServer_Server_destroy (filter1->server, &ev);
	      if (filter1->in_channel)
		VidServer_Channel_destroy (filter1->in_channel, &ev);
	    }
	}
      else
	{
	  fprintf(stderr, "Unknown command '%s'\n", buffer);
	  fprintf(stderr, "  spawn COMMAND [ARGS...]\n");
	  fprintf(stderr, "  connect PARENT CHILD\n");
	  fprintf(stderr, "  disconnect CHILD\n");
	  fprintf(stderr, "  kill server\n");
	  fprintf(stderr, "  quit\n");
	}
    }

  tmp_list = servers;
  while (tmp_list)
    {
      ServerInfo *info = tmp_list->data;

      VidServer_Server_destroy (info->server, &ev);
      if (info->in_channel)
	VidServer_Server_destroy (info->in_channel, &ev);

      tmp_list = tmp_list->next;
    }

  return 0;
}
