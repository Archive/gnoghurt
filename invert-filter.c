/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "filtermain.h"


static int width, height, rowstride;

void
set_size (int w, int h, int r)
{
	width = w;
	height = h;
	rowstride = r;
}

void
set_params (const char *str)
{
}

char *
get_params (void)
{
	return NULL;
}

void 
process_frame (void *in_addr, void *out_addr)
{
	int i, j;
	guint32 *src, *dest;

	src = in_addr;
	dest = out_addr;
	j = height * (rowstride / 4);

	for (i = 0; i < j;  i++) {
		*dest++ = 0xffffffff ^ *src++;
	}
}
