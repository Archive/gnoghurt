/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <glib.h>
#include <string.h>
#include <math.h>
#include "filtermain.h"

int width, height, rowstride;

int sqrts[256] = { 
0,1,1,2,2,2,2,3,3,3,3,3,3,4,4,4,4,4,4,4,4,5,5,5,5,5,5,5,5,5,5,6,6,6,6,6,6,6,6,6,6,6,6,7,7,7,7,7,7,7,7,7,7,7,7,7,7,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,11,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,12,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,13,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,14,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,15,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,
};

static int 
sqrtm (int x) 
{
  int r;
  int t;
  if (x == 0)
    return 0;

  r = 1;
  t = x;
  
  while (t & 0xFFFFFF00)
    {
      t >>= 2;
      r <<= 1;
    }
  r *= sqrts[t];
  //  r = (x/r + r)/2;
  //  r = (x/r + r)/2;

  return r+1;
}

static char lookup[65536];

void 
set_size (int w, int h, int r)
{
  int i;

  width = w;
  height = h;
  rowstride = r;

  for (i=0; i<65536; i++)
    lookup[i] = sqrt(i);
}

void
set_params (const char *str)
{
}

char *
get_params (void)
{
  return NULL;
}

void 
process_frame (void *in_addr, void *out_addr)
{
  int i, j;

  guchar *p1, *p2, *p3;
  guchar *q;
  int xgrad;
  int ygrad;

  for (i=1; i<height-1; i++)
    {
      p1 = in_addr + (i-1) * rowstride;
      p2 = in_addr + i * rowstride;
      p3 = in_addr + (i+1) * rowstride;
      q = out_addr + i * rowstride;

      q[0] = q[1] = q[2] = 0;
      
      for (j = 3; j<3*(width-1); j++)
	{
	  xgrad = ((int)p1[3] + 2*(int)p2[3] + (int)p3[3]) - ((int)p1[-3] + 2*(int)p2[-3] + (int)p3[-3]);
	  ygrad = ((int)p3[-3] + 2*(int)p3[0] + (int)p3[3]) - ((int)p1[-3] + 2*(int)p1[0] + (int)p1[3]);

	  *q = lookup[CLAMP((xgrad * xgrad + ygrad * ygrad) >> 2,0,65535)];

	  p1++;
	  p2++;
	  p3++;
	  q++;
	}
      
      q[0] = q[1] = q[2] = 0;
    }
}

