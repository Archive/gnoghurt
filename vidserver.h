/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef __VIDSERVER_H__
#define __VIDSERVER_H__

#include "VidServer.h"

typedef struct _VidServer VidServer;
typedef struct _VidChannel VidChannel;

typedef void (*VidServerSetParamsFunc) (const char *params, void *data);
typedef char * (*VidServerGetParamsFunc) (void *data);
typedef void (*VidServerAddSourceFunc) (VidServer_Channel src, void *data);
typedef void (*VidServerDestroyFunc) (void *data);

VidServer *vid_server_create (CORBA_ORB orb,
			      int width, int height, int rowstride,
			      VidServerGetParamsFunc get_params_func,
			      VidServerSetParamsFunc set_params_func,
			      VidServerAddSourceFunc add_source_func,
			      VidServerDestroyFunc destroy_func,
			      char *data);
void vid_server_set_size (VidServer *server, int width, int height, int rowstride);
VidServer_Server vid_server_get_obj (VidServer *server);
char  *vid_server_get_frame (VidServer *server, long seqno);
void   vid_server_write_frame (VidServer *server);
void   vid_server_unref (VidServer *server);
void   vid_server_write_ior (VidServer *server);

typedef void (*VidServerFrameFunc) (long seqno, char *addr, void *data);

VidChannel *vid_channel_open (CORBA_ORB orb, VidServer_Channel ch,
			      int *width, int *height, int *rowstride,
			      VidServerFrameFunc frame_cb,
			      VidServerDestroyFunc destroy_func,
			      void *data);
VidServer_Server vid_channel_get_servobj (VidChannel *channel);
void        vid_channel_frame_done (VidChannel *channel);
void        vid_channel_close (VidChannel *channel);

/* Utility functions */

CORBA_ORB vid_init (int *argc, char ***argv);
VidServer_Server vid_run_server (CORBA_ORB orb, char *exe);
VidChannel *vid_spawn_filter (CORBA_ORB orb,
			      VidServer_Channel in_channel,
			      VidServer_Server *out_server,
			      char **argv,
			      int *width, int *height, int *rowstride,
			      VidServerFrameFunc frame_cb,
			      VidServerDestroyFunc destroy_func,
			      void *data);
void       vid_filter_init (int *argc, char ***argv,
			    int *width, int *height, int *rowstride,
			    VidServer **server, VidChannel **channel,
			    VidServerFrameFunc frame_cb,
			    VidServerGetParamsFunc get_params_cb,
			    VidServerSetParamsFunc set_params_cb,
			    VidServerDestroyFunc destroy_func,
			    void *data);

VidServer_Server vid_server_ior_read (CORBA_ORB orb);

#endif __VIDSERVER_H__

