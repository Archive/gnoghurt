/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef FILTERS_H
#define FILTERS_H

#include <gdk_imlib.h>
#include <libgnomeui/gnome-canvas.h>
#include "vidserver.h"

typedef GtkWidget* (*FilterCreateControls) (VidServer_Server server);

typedef struct {
	char *name;
	char *exe;
	char **toolbar_icon;
	char **work_icon;
	FilterCreateControls controls_func;
} FilterSpec;

typedef struct _Filter Filter;
typedef struct _Link Link;

struct _Filter {
	FilterSpec *spec;

        VidServer_Server server;
	GnomeCanvasItem *item;

	Link *parent_link;
	GList *child_links;

	GtkWidget *controls_win;
};

struct _Link {
	GnomeCanvasItem *item;
	Filter *source;
	Filter *dest;
        VidServer_Channel channel;
};

extern FilterSpec filter_specs[];

Filter *filter_new (GnomeCanvasGroup *group, FilterSpec *fspec, GnomeCanvas *canvas,
		    double x, double y);
void filter_destroy (Filter *f);
void filter_move_to (Filter *f, double x, double y);

void filter_activate_link (Link *link);
void filter_show_controls (Filter *f);


#endif
