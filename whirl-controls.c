/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include <gtk/gtk.h>
#include "controls.h"

typedef struct _WhirlInfo WhirlInfo;

struct _WhirlInfo {
	VidServer_Server server;

	float angle;
};

static void
set_whirl_params (WhirlInfo *info)
{
	CORBA_Environment ev;
	char *params;

	CORBA_exception_init(&ev);

	params = g_strdup_printf("%f", info->angle);
	VidServer_Server_set_params (info->server, params, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_warning ("Could not fetch parameters\n");
	g_free (params);
}

static void
angle_changed (GtkAdjustment *adj, WhirlInfo *info)
{
	info->angle = adj->value;
	set_whirl_params(info);
}

static GtkWidget *
create_slider (const char *name, gint value, GtkSignalFunc cb, gpointer data)
{
	GtkAdjustment *adj;

	GtkWidget *hbox;
	GtkWidget *slider;
	GtkWidget *label;

	hbox = gtk_hbox_new (FALSE, 5);

	label = gtk_label_new (name);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	adj = GTK_ADJUSTMENT (gtk_adjustment_new (value * 100. / 65535., 
						  -720., 720., 1., 10., 10.));
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			    GTK_SIGNAL_FUNC (cb), data);

	slider = gtk_hscale_new (adj);
	gtk_box_pack_start (GTK_BOX (hbox), slider, TRUE, TRUE, 0);

	return hbox;
}

GtkWidget*
whirl_create_controls (VidServer_Server server)
{
	GtkWidget *slider;
	GtkWidget *vbox;
	char *params;
	CORBA_Environment ev;
	WhirlInfo *info = g_new (WhirlInfo, 1);

	CORBA_exception_init(&ev);

	info->server = server;

	params = VidServer_Server_get_params (server, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_warning ("Could not fetch parameters\n");
	else
		sscanf(params, "%f", &info->angle);
	/*  CORRBA_free (params); */ /* FIXME - how do we do this? */
	
	vbox = gtk_vbox_new (FALSE, 4);
  
	slider = create_slider ("Angle: ", info->angle, angle_changed, info);
	gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);
	
	return vbox;
}
