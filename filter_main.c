/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "vidserver.h"
#include <stdio.h>
#include <unistd.h>

GMainLoop *loop;
int width, height, rowstride;
VidServer *server;
VidChannel *channel;

long cur_seqno;

gboolean
process_frame (gpointer data)
{
  char *in_addr = data;
  char *out_addr = vid_server_get_frame (server, cur_seqno);
  
  memcpy (out_addr, in_addr, rowstride * height);
  vid_server_write_frame (server);
  
  vid_channel_frame_done (channel);

  return FALSE;
}

#define BUFINCR 1024

static gboolean
in_handler (GIOChannel *source, GIOCondition condition, gpointer data)
{
  static char *buffer;
  static gint buflen = 0;
  static gint bufoffset = 0;
  gint count;
  char *p, *q;

  if (buflen == bufoffset)
    {
      buflen += BUFINCR;
      buffer = g_realloc (buffer, buflen);
    }
  
  count = read (0, &buffer[bufoffset], buflen - bufoffset - 1);
  if (count == 0)
    {
      g_main_quit (data);
      return FALSE;
    }
  
  buflen += count;
  buffer[bufoffset + count] = '\0';

  p = buffer;
  while ((q = strchr (p, '\n')))
    {
      *q = '\0';
      parameter_handler (p);
      p = q + 1;
    }
  
  return TRUE;
}

static void 
frame_received (long seqno, char *addr, void *data)
{
  cur_seqno = seqno;
  g_idle_add (process_frame, addr);
}

static void 
destroy_func (void *data)
{
  g_main_quit(loop);
}

int 
main (int argc, char **argv)
{
  GIOChannel *channel

  vid_filter_init (&argc, &argv,
		   &width, &height, &rowstride,
		   &server, &channel,
		   frame_received, destroy_func, NULL);

  loop = g_main_new (FALSE);

  channel = g_io_channel_unix_new (0);
  g_io_add_watch (channel, G_IO_IN | G_IO_HUP, in_handler, loop);
  g_io_channel_unref (channel);
    
  g_main_run (loop);
  g_main_destroy (loop);

  vid_channel_close (channel);
  vid_server_unref (server);

  return 0;
}


