/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <fcntl.h>
#include <errno.h>
#include <stdio.h>

#include "vidserver.h"

#define ulong gulong
#include <linux/videodev.h>

gint vidfd;
long width, height, rowstride;
long sequence = 0;
gboolean double_scan = FALSE;

gchar *vidbuf;

VidServer *server;

static char *
get_params (void *data)
{
  char buffer[1024];
  struct video_picture pict;

  ioctl (vidfd, VIDIOCGPICT, &pict);
  sprintf(buffer, "%d %d %d %d %d", pict.brightness, pict.hue, pict.colour, pict.contrast, double_scan);

  g_print("%s\n", buffer);
  
  return CORBA_string_dup (buffer);
}

static void
set_params (const char *params, void *data)
{
  int brightness, hue, color, contrast, t;

  if (sscanf (params, "%d %d %d %d", &brightness, &hue, &color, &contrast) == 4)
    {
      struct video_picture pict;

      ioctl (vidfd, VIDIOCGPICT, &pict);
      
      pict.brightness = brightness;
      pict.hue = hue;
      pict.colour = color;
      pict.contrast = contrast;

      ioctl (vidfd, VIDIOCSPICT, &pict);
    }

  sscanf (params, "%d %d %d %d %d", &t, &t, &t , &t, &double_scan);
}

void
bgr2rgb (char *out_addr, char *in_addr)
{
  int i, j;
  for (i=0; i<height; i++)
    {
      char *q = out_addr + i * rowstride;
      char *p = in_addr + i * rowstride;

      for (j=0; j<width; j++)
	{
	  q[2] = p[0];
	  q[1] = p[1];
	  q[0] = p[2];
	  
	  q += 3;
	  p += 3;
	}
    }
}

void
doublecopy (char *out_addr, char *in_addr)
{
  int i;

  for (i=0; i<height; i+=2)
    {
      memcpy (out_addr + rowstride * i, in_addr + rowstride * i, rowstride);
      memcpy (out_addr + rowstride * (i+1), in_addr + rowstride * i, rowstride);
    }
}

int 
main (int argc, char **argv)
{
  struct timeval tv;
  /* struct timeval last_tv; */
  char *out_addr;

#if 0
  GMainLoop *loop;
  GIOChannel *channel;
#endif
  
  CORBA_ORB orb;
  CORBA_Environment ev;
  
  struct video_capability caps;
  struct video_window capwin;
  struct video_mbuf capmbuf;
  struct video_mmap capmmap;
  struct video_tuner captuner;
  struct video_channel vchan;

  int curframe;


  CORBA_exception_init(&ev);
  orb = vid_init (&argc, &argv);

  vidfd = open("/dev/video0", O_RDONLY);
  if (vidfd < 0)
    g_error ("Cannot open video device: %s\n", g_strerror (errno));

  ioctl (vidfd, VIDIOCGCAP, &caps);

  g_print ("Found %s max size (%d, %d)\n",
	   caps.name, caps.maxwidth, caps.maxheight);

  ioctl (vidfd, VIDIOCGWIN, &capwin);

  capwin.x = 0;
  capwin.y = 0;
  capwin.width = 640;
  capwin.height = 480;
  capwin.clipcount = 0;
  capwin.flags = 0;

  ioctl (vidfd, VIDIOCSWIN, &capwin);
  ioctl (vidfd, VIDIOCGWIN, &capwin);

  g_print ("Using (%d, %d)\n", capwin.width, capwin.height);

  captuner.tuner = 0;
  ioctl (vidfd, VIDIOCGFREQ, &captuner);
  captuner.mode = VIDEO_MODE_NTSC;
  ioctl (vidfd, VIDIOCSFREQ, &captuner);
  ioctl (vidfd, VIDIOCGCHAN, &vchan);
  vchan.norm = VIDEO_MODE_NTSC;
  vchan.channel = 0;
  ioctl (vidfd, VIDIOCSCHAN, &vchan);

  ioctl (vidfd, VIDIOCGMBUF, &capmbuf);
  g_print ("MMapping buf of size %d (%d frames)\n", capmbuf.size, capmbuf.frames);

  vidbuf = mmap (0, capmbuf.size, PROT_READ, MAP_SHARED, vidfd, 0);
  if (vidbuf == (void *)-1)
    g_error ("MMap failed, %s\n", g_strerror (errno));

  width = capwin.width;
  height = capwin.height;
  rowstride = capwin.width * 3;

  server = vid_server_create (orb, width, height, rowstride, get_params, set_params, NULL, NULL, NULL);
  vid_server_write_ior (server);

  capmmap.height = height;
  capmmap.width = width;
  capmmap.format = VIDEO_PALETTE_RGB24;
  
  capmmap.frame = 0;
  ioctl (vidfd, VIDIOCMCAPTURE, &capmmap);
  capmmap.frame = 1;
  ioctl (vidfd, VIDIOCMCAPTURE, &capmmap);

  gettimeofday (&tv, NULL);
  curframe = 0;
  out_addr = g_malloc (height * rowstride);
  while (1)
    {
      /* Wait for frame */
      if (ioctl (vidfd, VIDIOCSYNC, &curframe) < 0)
	g_print ("Unused\n");
      
      while (g_main_iteration (FALSE))
	/* Nothing */;

      // last_tv = tv;
      //gettimeofday (&tv, NULL);
      //g_print ("%f\n", 1000000. / (1000000*(tv.tv_sec - last_tv.tv_sec) + tv.tv_usec  - last_tv.tv_usec));
      
      out_addr = vid_server_get_frame (server, ++sequence);
      
      if (double_scan)
	doublecopy (out_addr, vidbuf + capmbuf.offsets[curframe]);
      else
	memcpy (out_addr, vidbuf + capmbuf.offsets[curframe], rowstride * height);
      vid_server_write_frame (server);

      //last_tv = tv;
      //gettimeofday (&tv, NULL);
      //g_print ("== %f ==\n", 1000000. / (1000000*(tv.tv_sec - last_tv.tv_sec) + tv.tv_usec  - last_tv.tv_usec));

      capmmap.frame = curframe;
      ioctl (vidfd, VIDIOCMCAPTURE, &capmmap);

      curframe = (curframe + 1) % capmbuf.frames;
    }
  
#if 0
  channel = g_io_channel_unix_new (vidfd);
  g_io_add_watch (channel, G_IO_IN, vid_in, NULL);
  g_io_channel_unref (channel);

  loop = g_main_new (FALSE);
  g_main_run (loop);
  g_main_destroy (loop);
#endif  

  vid_server_unref (server);

  close (vidfd);

  return 0;
}


