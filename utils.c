/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <glib.h>
#include <errno.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/wait.h>

#include "vidserver.h"

static gboolean
handler_func (GIOChannel *source, GIOCondition condition, gpointer data)
{
  if(condition & (G_IO_HUP|G_IO_NVAL|G_IO_ERR))
    giop_main_handle_connection_exception(data);
  else
    giop_main_handle_connection(data);

  return TRUE;
}

static void 
add_connection_handler (GIOPConnection *newcnx)
{
  guint tag;
  GIOChannel *channel = g_io_channel_unix_new (GIOP_CONNECTION_GET_FD (newcnx));

  tag = g_io_add_watch (channel, G_IO_IN | G_IO_ERR | G_IO_HUP | G_IO_NVAL,
			handler_func, newcnx);
  g_io_channel_unref (channel);
  
  newcnx->user_data = GUINT_TO_POINTER(tag);
}

static void 
remove_connection_handler (GIOPConnection *oldcnx)
{
  g_source_remove (GPOINTER_TO_UINT (oldcnx->user_data));
  oldcnx->user_data = GUINT_TO_POINTER((guint)-1);
}

CORBA_ORB 
vid_init (int *argc, char ***argv)
{
  CORBA_Environment ev;
  CORBA_ORB orb;
  PortableServer_POA poa;
  
  CORBA_exception_init(&ev);

  IIOPAddConnectionHandler = add_connection_handler;
  IIOPRemoveConnectionHandler = remove_connection_handler;

  orb = CORBA_ORB_init(argc, *argv, "orbit-local-orb", &ev);
  
  poa = (PortableServer_POA)CORBA_ORB_resolve_initial_references(orb, "RootPOA", &ev);
  PortableServer_POAManager_activate(PortableServer_POA__get_the_POAManager(poa, &ev), &ev);
  
  return orb;
}

VidServer_Server 
vid_run_server (CORBA_ORB orb, char *exe)
{
  CORBA_Environment ev;

  VidServer_Server servobj;
  char buf[1024];
  int out_fds[2];
  int pid;
  int i, count;
  char **argv;

  argv = g_strsplit (exe, " ", -1);

  CORBA_exception_init(&ev);
  
  pipe (out_fds);

  pid = fork();
  if (pid == 0) /* Child */
    {
      close (1);
      dup2 (out_fds[1], 1);

      pid = fork();
      if (pid == 0) /* grandhild */
	{
	  execvp (argv[0], argv);

	  g_error ("Cannot exec filter: %s\n", g_strerror (errno));
	}
      else if (pid < 0)
	g_error ("Cannot fork in child: %s\n", g_strerror (errno));

      _exit (0);
    }
  else if (pid < 0)
    g_error ("Cannot fork: %s\n", g_strerror (errno));

  /* Parent */

  g_strfreev (argv);
  close (out_fds[1]);
  waitpid (pid, NULL, 0);

  count = read (out_fds[0], &buf, sizeof(buf) - 1);
  if (count <= 0)
    return NULL;

  for (i=0; buf[i]; i++)
    if (buf[i] == '\n')
      {
	buf[i] = '\0';
	break;
      }

  buf[count] = '\0';
  close (out_fds[0]);

  servobj = CORBA_ORB_string_to_object (orb, buf, &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    g_error ("Could not convert IOR to object\n");

  return servobj;
}

void
vid_filter_init (int *argc, char ***argv,
		 int *width, int *height, int *rowstride,
		 VidServer **server, VidChannel **channel,
		 VidServerFrameFunc frame_cb,
		 VidServerGetParamsFunc get_params_cb,
		 VidServerSetParamsFunc set_params_cb,
		 VidServerDestroyFunc destroy_func,
		 void *data)
{
  CORBA_ORB orb;
  CORBA_Environment ev;
  
  VidServer_Server servobj;
  VidServer_Channel chobj;

  char *ior;

  CORBA_exception_init(&ev);
  orb = vid_init (argc, argv);

  /* Set up channel to upstream */

  if (*argc != 2)
    {
      g_print ("Usage: FILTER IOR\n");
      exit (1);
    }

  chobj = CORBA_ORB_string_to_object (orb, (*argv)[1], &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    g_error ("Could not convert IOR to object\n");

  *channel = vid_channel_open (orb, chobj,
			       width, height, rowstride,
			       frame_cb, destroy_func, data);

  CORBA_Object_release (chobj, &ev);
  
  /* Set up server */

  *server = vid_server_create (orb, *width, *height, *rowstride,
			       get_params_cb, set_params_cb, NULL, NULL, data);
  servobj = vid_server_get_obj (*server);

  ior = CORBA_ORB_object_to_string (orb, servobj, &ev);
  g_print ("%s\n", ior);
  CORBA_free (ior);

  CORBA_Object_release (servobj, &ev);
}

#define BUFSIZE 1024

VidServer_Server 
vid_server_ior_read (CORBA_ORB orb)
{
  CORBA_Environment ev;
  VidServer_Server server;
  char *filename;
  int fd;
  char buffer[BUFSIZE];
  int i;

  filename = g_strconcat (g_get_home_dir(), "/", ".vidserver.ior", NULL);

  fd = open (filename, O_RDONLY);
  if (fd < 0)
    g_error ("Could not open '%s': %s", filename, g_strerror (errno));

  read (fd, buffer, BUFSIZE - 1);
  buffer[BUFSIZE - 1] = '\0';

  for (i=0; i<BUFSIZE-1; i++)
    {
      if (buffer[i] == '\n')
	buffer[i] = '\0';
    }
  close (fd);
  
  CORBA_exception_init(&ev);

  server = CORBA_ORB_string_to_object (orb, buffer, &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    g_error ("Could not convert IOR to object\n");

  g_free (filename);

  return server;
}

