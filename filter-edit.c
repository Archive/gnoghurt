/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gnome.h>
#include "filter-edit.h"

#include "images/move-tool.xpm"
#if 0
#include "images/link-tool.xpm"
#endif


static void filter_edit_class_init (FilterEditClass *class);
static void filter_edit_init (FilterEdit *fe);
static void filter_edit_destroy (GtkObject *object);

static GnomeAppClass *parent_class;


GtkType
filter_edit_get_type (void)
{
	static GtkType filter_edit_type = 0;

	if (!filter_edit_type) {
		static const GtkTypeInfo filter_edit_info = {
			"FilterEdit",
			sizeof (FilterEdit),
			sizeof (FilterEditClass),
			(GtkClassInitFunc) filter_edit_class_init,
			(GtkObjectInitFunc) filter_edit_init,
			NULL, /* reserved_1 */
			NULL, /* reserved_2 */
			(GtkClassInitFunc) NULL
		};

		filter_edit_type = gtk_type_unique (gnome_app_get_type (), &filter_edit_info);
	}

	return filter_edit_type;
}

static void
filter_edit_class_init (FilterEditClass *class)
{
	GtkObjectClass *object_class;

	object_class = GTK_OBJECT_CLASS (class);

	parent_class = gtk_type_class (gnome_app_get_type ());

	object_class->destroy = filter_edit_destroy;
}

static void
exit_cb (GtkWidget *widget, gpointer data)
{
	gtk_widget_destroy (GTK_WIDGET (data));
}

static void
about_cb (GtkWidget *widget, gpointer data)
{
	const char *authors[] = {
		"Federico Mena <federico@redhat.com>",
		"Owen Taylor <otaylor@redhat.com>",
		NULL
	};
	GtkWidget *about;

	about = gnome_about_new ("Video Filter Editor",
				 "0.1",
				 "Copyright (C) 1999 Red Hat, Inc.",
				 authors,
				 "This is a simple program to edit a video filtering "
				 "pipeline.",
				 NULL);

	gnome_dialog_run_and_close (GNOME_DIALOG (about));
}

static void
move_tool_cb (GtkWidget *widget, gpointer data)
{
	FilterEdit *fe;

	fe = FILTER_EDIT (data);

	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
		fe->tool_func = NULL;
	else
		fe->tool_func = move_tool_event;
}

static void
link_tool_cb (GtkWidget *widget, gpointer data)
{
	FilterEdit *fe;

	fe = FILTER_EDIT (data);

	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
		fe->tool_func = NULL;
	else
		fe->tool_func = link_tool_event;
}

static void
delete_tool_cb (GtkWidget *widget, gpointer data)
{
	FilterEdit *fe;

	fe = FILTER_EDIT (data);

	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
		fe->tool_func = NULL;
	else
		fe->tool_func = delete_tool_event;
}

static void
filter_tool_cb (GtkWidget *widget, gpointer data)
{
	FilterEdit *fe;
	FilterSpec *fspec;

	fe = FILTER_EDIT (data);

	if (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (widget)))
		fe->current_spec = NULL;
	else {
		fspec = gtk_object_get_data (GTK_OBJECT (widget), "filter-spec");
		g_assert (fspec != NULL);
		fe->current_spec = fspec;
	}
}

static GnomeUIInfo toolbar[] = {
	GNOMEUIINFO_RADIOLIST (NULL),
	GNOMEUIINFO_END
};

static void
create_toolbar (FilterEdit *fe)
{
	FilterSpec *fspec;
	int i, j, n;
	GnomeUIInfo *uiinfo;

	/* Count number of filters */

	n = 0;

	for (fspec = filter_specs; fspec->name; fspec++)
		n++;

	/* Create toolbar info */

	uiinfo = g_new0 (GnomeUIInfo, n + 4);

	/* Basic tools */

	uiinfo[0].type = GNOME_APP_UI_ITEM;
	uiinfo[0].label = "Move";
	uiinfo[0].hint = "Move a filter node";
	uiinfo[0].moreinfo = move_tool_cb;
	uiinfo[0].pixmap_type = GNOME_APP_PIXMAP_DATA;
	uiinfo[0].pixmap_info = move_tool_xpm;

	uiinfo[1].type = GNOME_APP_UI_ITEM;
	uiinfo[1].label = "Link";
	uiinfo[1].hint = "Link two nodes together";
	uiinfo[1].moreinfo = link_tool_cb;
	uiinfo[1].pixmap_type = GNOME_APP_PIXMAP_STOCK;
	uiinfo[1].pixmap_info = GNOME_STOCK_PIXMAP_JUMP_TO;

	uiinfo[2].type = GNOME_APP_UI_ITEM;
	uiinfo[2].label = "Delete";
	uiinfo[2].hint = "Delete a link or filter node";
	uiinfo[2].moreinfo = delete_tool_cb;
	uiinfo[2].pixmap_type = GNOME_APP_PIXMAP_STOCK;
	uiinfo[2].pixmap_info = GNOME_STOCK_PIXMAP_TRASH;

	/* Filter tools */

	for (i = 0, j = 3; i < n; i++, j++) {
		uiinfo[j].type = GNOME_APP_UI_ITEM;
		uiinfo[j].label = filter_specs[i].name;
		uiinfo[j].hint = NULL;
		uiinfo[j].moreinfo = filter_tool_cb;
		uiinfo[j].pixmap_type = GNOME_APP_PIXMAP_DATA;
		uiinfo[j].pixmap_info = filter_specs[i].toolbar_icon;
	}

	uiinfo[j].type = GNOME_APP_UI_ENDOFINFO;

	/* Create toolbar */

	toolbar[0].moreinfo = uiinfo;

	gnome_app_create_toolbar_with_data (GNOME_APP (fe), toolbar, fe);

	/* Save the tool buttons */

	fe->move_button = uiinfo[0].widget;
	fe->link_button = uiinfo[1].widget;
	fe->delete_button = uiinfo[2].widget;

	/* Set the filter data */

	for (i = 0, j = 3; i < n; i++, j++)
		gtk_object_set_data (GTK_OBJECT (uiinfo[j].widget), "filter-spec", &filter_specs[i]);

	g_free (uiinfo);
}

static gint
item_event (GnomeCanvasItem *item, GdkEvent *event, gpointer data)
{
	FilterEdit *fe;

	fe = FILTER_EDIT (data);

	if (fe->tool_func)
		return (* fe->tool_func) (item, event, data);
	else
		return FALSE;
}

static gint
button_press_cb (GtkWidget *widget, GdkEventButton *event, gpointer data)
{
	FilterEdit *fe;
	Filter *f;
	double x, y;

	fe = FILTER_EDIT (data);
	if (!(fe->current_spec && event->type == GDK_BUTTON_PRESS &&  event->button == 1))
		return FALSE;

	/* Create the filter object */

	gnome_canvas_window_to_world (GNOME_CANVAS (fe->canvas),
				      event->x, event->y,
				      &x, &y);

	f = filter_new (fe->filter_group, fe->current_spec, GNOME_CANVAS (fe->canvas), x, y);
	gtk_signal_connect (GTK_OBJECT (f->item), "event",
			    GTK_SIGNAL_FUNC (item_event),
			    fe);

	fe->filters = g_list_prepend (fe->filters, f);

	/* Make the move tool active */
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (fe->move_button), TRUE);

	gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "button_press_event");
	return TRUE;
}

static void
create_canvas (FilterEdit *fe)
{
	gtk_widget_push_visual (gdk_imlib_get_visual ());
	gtk_widget_push_colormap (gdk_imlib_get_colormap ());

	fe->canvas = gnome_canvas_new ();

	gtk_widget_pop_visual ();
	gtk_widget_pop_colormap ();

	gtk_signal_connect (GTK_OBJECT (fe->canvas), "button_press_event",
			    GTK_SIGNAL_FUNC (button_press_cb),
			    fe);

	fe->link_group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (
		gnome_canvas_root (GNOME_CANVAS (fe->canvas)),
		gnome_canvas_group_get_type (),
		NULL));

	fe->filter_group = GNOME_CANVAS_GROUP (gnome_canvas_item_new (
		gnome_canvas_root (GNOME_CANVAS (fe->canvas)),
		gnome_canvas_group_get_type (),
		NULL));

	gnome_app_set_contents (GNOME_APP (fe), fe->canvas);
	gtk_widget_show (fe->canvas);
}

static GnomeUIInfo file_menu[] = {
	GNOMEUIINFO_MENU_EXIT_ITEM (exit_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo help_menu[] = {
	GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, NULL),
	GNOMEUIINFO_END
};

static GnomeUIInfo main_menu[] = {
	GNOMEUIINFO_MENU_FILE_TREE (file_menu),
	GNOMEUIINFO_MENU_HELP_TREE (help_menu),
	GNOMEUIINFO_END
};

static void
filter_edit_init (FilterEdit *fe)
{
	gnome_app_construct (GNOME_APP (fe), "filter-edit", "Video Filter Editor");

	gnome_app_create_menus_with_data (GNOME_APP (fe), main_menu, fe);
	create_toolbar (fe);

	create_canvas (fe);
}

static void
filter_edit_destroy (GtkObject *object)
{
	FilterEdit *fe;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_FILTER_EDIT (object));

	fe = FILTER_EDIT (object);

	g_list_foreach (fe->filters, (GFunc)filter_destroy, NULL);
	g_list_free (fe->filters);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

GtkWidget *
filter_edit_new (void)
{
	return GTK_WIDGET (gtk_type_new (filter_edit_get_type ()));
}
