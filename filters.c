/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gnome.h>
#include "filters.h"
#include "filter-edit.h"
#include "controls.h"

#include "images/whirl-filter-tb.xpm"
#include "images/whirl-filter.xpm"
#include "images/edge-filter-tb.xpm"
#include "images/edge-filter.xpm"
#include "images/echo-filter-tb.xpm"
#include "images/echo-filter.xpm"
#include "images/emboss-filter-tb.xpm"
#include "images/emboss-filter.xpm"
#include "images/invert-filter-tb.xpm"
#include "images/invert-filter.xpm"
#include "images/video.xpm"
#include "images/video-tb.xpm"
#include "images/monitor.xpm"
#include "images/monitor-tb.xpm"

FilterSpec filter_specs[] = {
	{ "Whirl", "./whirl-filter", whirl_filter_tb_xpm, whirl_filter_xpm, whirl_create_controls },
	{ "Edge", "./edge-filter", edge_filter_tb_xpm, edge_filter_xpm },
	{ "Echo", "./echo-filter", echo_filter_tb_xpm, echo_filter_xpm },
	{ "Emboss", "./emboss-filter", emboss_filter_tb_xpm, emboss_filter_xpm },
	{ "Invert", "./invert-filter", invert_filter_tb_xpm, invert_filter_xpm, NULL },
	{ "Video", NULL, video_tb_xpm, video_xpm, video_create_controls },
	{ "Monitor", "./monitor", monitor_tb_xpm, monitor_xpm, NULL },
	{ NULL, NULL, NULL, NULL }
};

static void
filter_event (GnomeCanvasItem *item, GdkEvent *event, Filter *f)
{
	if (event->type == GDK_BUTTON_PRESS &&
	    event->button.button == 3)
		filter_show_controls (f);
}

Filter *
filter_new (GnomeCanvasGroup *group, FilterSpec *fspec, GnomeCanvas *canvas, double x, double y)
{
	Filter *f;
	GdkImlibImage *im;

	f = g_new0 (Filter, 1);

	f->spec = fspec;

	im = gdk_imlib_create_image_from_xpm_data (fspec->work_icon);

	f->item = gnome_canvas_item_new (group,
					 gnome_canvas_image_get_type (),
					 "image", im,
					 "x", x,
					 "y", y,
					 "width", (double) im->rgb_width,
					 "height", (double) im->rgb_height,
					 "anchor", GTK_ANCHOR_CENTER,
					 NULL);
	gtk_object_set_data (GTK_OBJECT (f->item), "filter", f);

	gtk_signal_connect (GTK_OBJECT (f->item),
			    "event",
			    GTK_SIGNAL_FUNC (filter_event),
			    f);

	if (fspec->exe)
		f->server = vid_run_server (orb, fspec->exe);
	else
		f->server = vid_server_ior_read (orb);

	return f;
}

static void
link_destroy (Link *link)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);
	VidServer_Channel_destroy (link->channel, &ev);

	gtk_object_destroy (GTK_OBJECT (link->item));
}

void
filter_destroy (Filter *f)
{
	GList *l;
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	gtk_object_destroy (GTK_OBJECT (f->item));

	if (f->parent_link)
		link_destroy (f->parent_link);

	for (l = f->child_links; l; l = l->next)
		link_destroy (l->data);

	g_list_free (f->child_links);
	g_free (f);
}

static void
move_link (Link *link, int move_source, double x, double y)
{
	GtkArg arg;
	GnomeCanvasPoints *points;

	arg.name = "points";
	gtk_object_getv (GTK_OBJECT (link->item), 1, &arg);
	points = GTK_VALUE_BOXED (arg);

	if (move_source) {
		points->coords[0] = x;
		points->coords[1] = y;
	} else {
		points->coords[2] = x;
		points->coords[3] = y;
	}

	gnome_canvas_item_set (link->item,
			       "points", points,
			       NULL);
	gnome_canvas_points_free (points);
}

void
filter_move_to (Filter *f, double x, double y)
{
	GList *l;

	gnome_canvas_item_set (f->item, "x", x, "y", y, NULL);

	if (f->parent_link)
		move_link (f->parent_link, FALSE, x, y);

	for (l = f->child_links; l; l = l->next)
		move_link (l->data, TRUE, x, y);
}

void
filter_activate_link (Link *link)
{
	CORBA_Environment ev;

	CORBA_exception_init (&ev);

	link->source->child_links = g_list_prepend (link->source->child_links, link);
	link->dest->parent_link = link;

	link->channel = VidServer_Server_create_channel (link->source->server, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_error ("Could not create channel\n");

	VidServer_Server_add_source (link->dest->server, link->channel, &ev);
	if (ev._major != CORBA_NO_EXCEPTION)
		g_error ("Could not connect filters\n");
}

void
filter_show_controls (Filter *f)
{
	if (!f->controls_win) {
		f->controls_win = gtk_window_new (GTK_WINDOW_TOPLEVEL);
		gtk_window_set_title (GTK_WINDOW (f->controls_win), f->spec->name);

		gtk_signal_connect (GTK_OBJECT (f->controls_win),
				    "delete_event",
				    GTK_SIGNAL_FUNC (gtk_widget_hide_on_delete),
				    NULL);

		if (f->spec->controls_func) {
			GtkWidget *controls;
			controls = f->spec->controls_func (f->server);
			gtk_container_add (GTK_CONTAINER (f->controls_win), controls);
		}
	}

	gtk_widget_show_all (f->controls_win);
}
