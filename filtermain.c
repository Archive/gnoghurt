/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gtk/gtk.h>
#include <stdio.h>
#include <gdk/gdkx.h>
#include <X11/extensions/XShm.h>
#include "vidserver.h"
#include "filtermain.h"

typedef struct _FilterInfo FilterInfo;

char *paramstr = NULL;

struct _FilterInfo {
  VidChannel *channel;
  char *addr;
  long seqno;
};

static void frame_received (long seqno, char *addr, void *data);
static void channel_destroyed (void *data);

int frames;

int width, height, rowstride;

CORBA_ORB orb;
VidServer *server;

GList *filters = NULL;

gboolean
do_frame (gpointer data)
{
  FilterInfo *info = data;

  char *out_addr = vid_server_get_frame (server, info->seqno);

  while (g_main_iteration (FALSE))
    /* Nothing */;

  process_frame (info->addr, out_addr);
  vid_server_write_frame (server);
  
  vid_channel_frame_done (info->channel);

  return FALSE;
}

static void 
frame_received (long seqno, char *addr, void *data)
{
  FilterInfo *info = data;
  static long last_seqno = -1;

  if (last_seqno != -1 && seqno < last_seqno)
    {
      /* Just discard out-of-order frames for now */
      vid_channel_frame_done (info->channel);
      return;
    }

  frames++;

  last_seqno = seqno;

  info->addr = addr;
  info->seqno = seqno;
  gtk_idle_add (do_frame, info);
}

static void 
channel_destroyed (void *data)
{
  FilterInfo *info = data;
  
  filters = g_list_remove (filters, info);
  g_free (info);
}

void add_source (VidServer_Channel src, void *data)
{
  gint w, h, r;
  FilterInfo *info = g_new (FilterInfo, 1);

  info->channel = vid_channel_open (orb, src, &w, &h, &r, frame_received, channel_destroyed, info);

  if (filters)
    {
      if (w != width || h != height || r != rowstride)
	{
	  g_warning ("Mixed size sources!\n");
	  vid_channel_close (info->channel);
	  g_free (info);
	  return;
	}
    }
  else
    {
      width = w;
      height = h;
      rowstride = r;

      vid_server_set_size (server, width, height, rowstride);      
      set_size (width, height, rowstride);
    }

  filters = g_list_prepend (filters, info);
}

static void 
server_destroy (void *data)
{
  gtk_main_quit();
}

static void
set_params_func (const char *params, void *data)
{
  set_params (params);
}

static char *
get_params_func (void *data)
{
  char *params = get_params();
  char *retval;

  if (params)
    {
      retval = CORBA_string_dup (params);
      g_free (params);
      return retval;
    }
  else
    return CORBA_string_dup ("");
}

int 
main (int argc, char **argv)
{
  CORBA_Environment ev;
  GList *tmp_list;

  VidServer_Server servobj;
  char *ior;

  orb = vid_init (&argc, &argv);
  gtk_init (&argc, &argv);

  CORBA_exception_init(&ev);
  
  width = 640;
  height = 480;
  rowstride = width*3;

  server = vid_server_create (orb, width, height, rowstride,
			      get_params_func,
			      set_params_func,
			      add_source, server_destroy, NULL);

  servobj = vid_server_get_obj (server);

  ior = CORBA_ORB_object_to_string (orb, servobj, &ev);
  g_print ("%s\n", ior);
  CORBA_free (ior);

  CORBA_Object_release (servobj, &ev);

  gtk_main ();

  tmp_list = filters;
  while (tmp_list)
    {
      FilterInfo *filter = tmp_list->data;
      tmp_list = tmp_list->next;

      vid_channel_close (filter->channel);
    }

  return 0;
}
