/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <stdio.h>
#include "vidserver.h"
#include "clientmain.h"


static void
value_changed (GtkAdjustment *adj, gpointer data)
{
	char buf[100];

	snprintf (buf, 100, "%g", adj->value);
	set_params (buf);
}

GtkWidget *
create_controls (void)
{
	GtkAdjustment *adj;
	GtkWidget *scale;

	adj = GTK_ADJUSTMENT (gtk_adjustment_new (1, 0, 7, 1, 1, 1));
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			    GTK_SIGNAL_FUNC (value_changed),
			    NULL);

	scale = gtk_hscale_new (adj);

	return scale;
}

char *
get_filter_command (void)
{
	return "./echo-filter";
}
