/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>
#include "filtermain.h"

/* This needs to be global, since set_params can be called
 * before set_size
 */
double global_angle = 0.0;

typedef struct {
	int width, height;
	int rowstride;
	int hwidth, hheight;
	int scale_num, scale_denom;
	int scale_x, scale_y;
	int radius, radius2;
	int *sqrt_lut;
	int *trig_lut;
	int *scale_lut;
	int *unscale_lut;
} WhirlContext;


static WhirlContext *the_wc;


static void
compute_sqrt_lut (WhirlContext *wc)
{
	int i;

	for (i = 0; i < wc->radius2; i++)
		wc->sqrt_lut[i] = sqrt (i);
}

static void
compute_scale_unscale_lut (WhirlContext *wc)
{
	int i;
	int num;

	num = MAX (wc->hwidth, wc->hheight);

	for (i = 0; i < num; i++) {
		wc->scale_lut[i] = i * wc->scale_num / wc->scale_denom;
		wc->unscale_lut[i] = i * wc->scale_denom / wc->scale_num;
	}
}

static WhirlContext *
whirl_context_new (int width, int height, int rowstride)
{
	WhirlContext *wc;

	wc = g_new (WhirlContext, 1);
	wc->width = width;
	wc->height = height;
	wc->rowstride = rowstride;

	wc->hwidth = wc->width / 2;
	wc->hheight = wc->height / 2;

	if (wc->hwidth < wc->hheight) {
		wc->scale_num = wc->hheight;
		wc->scale_denom = wc->hwidth;
		wc->scale_x = TRUE;
		wc->scale_y = FALSE;
	} else if (wc->hwidth > wc->hheight) {
		wc->scale_num = wc->hwidth;
		wc->scale_denom = wc->hheight;
		wc->scale_x = FALSE;
		wc->scale_y = TRUE;
	} else {
		wc->scale_num = 1;
		wc->scale_denom = 1;
		wc->scale_x = FALSE;
		wc->scale_y = FALSE;
	}

	wc->radius = MAX (wc->hwidth, wc->hheight);
	wc->radius2 = wc->radius * wc->radius;

	wc->sqrt_lut = g_new (int, wc->radius2);
	compute_sqrt_lut (wc);

	wc->trig_lut = g_new (int, 2 * wc->radius);

	wc->scale_lut = g_new (int, MAX (wc->hwidth, wc->hheight));
	wc->unscale_lut = g_new (int, MAX (wc->hwidth, wc->hheight));
	compute_scale_unscale_lut (wc);

	return wc;
}

static void
whirl_context_free (WhirlContext *wc)
{
	g_free (wc->sqrt_lut);
	g_free (wc->trig_lut);
	g_free (wc->scale_lut);
	g_free (wc->unscale_lut);
	g_free (wc);
}

static void
compute_trig_lut (WhirlContext *wc, double angle)
{
	int i;
	double factor, ang;

	for (i = 0; i < wc->radius; i++) {
		factor = 1.0 - (double) i / wc->radius;
		ang = angle * factor * factor;

		wc->trig_lut[2 * i] = (int) (sin (ang) * 100000 + 0.5);
		wc->trig_lut[2 * i + 1] = (int) (cos (ang) * 100000 + 0.5);
	}
}

static int
get_whirl_coords (WhirlContext *wc, int wx, int wy, int *x, int *y)
{
	int dx, dy;
	int d;
	int sina, cosa;
	int tx, ty;

	dx = (wx - wc->hwidth);
	dy = (wy - wc->hheight);

	if (wc->scale_x) {
		if (dx < 0)
			dx = -wc->scale_lut[-dx];
		else
			dx = wc->scale_lut[dx];
	}

	if (wc->scale_y) {
		if (dy < 0)
			dy = -wc->scale_lut[-dy];
		else
			dy = wc->scale_lut[dy];
	}

	d = dx * dx + dy * dy;

	if (d < wc->radius2) {
		d = wc->sqrt_lut[d];

		sina = wc->trig_lut[d << 1];
		cosa = wc->trig_lut[(d << 1) + 1];

		tx = (cosa * dx - sina * dy) / 100000;
		ty = (sina * dx + cosa * dy) / 100000;

		if (wc->scale_x) {
			if (tx < 0)
				tx = -wc->unscale_lut[-tx];
			else
				tx = wc->unscale_lut[tx];
		}

		if (wc->scale_y) {
			if (ty < 0)
				ty = -wc->unscale_lut[-ty];
			else
				ty = wc->unscale_lut[ty];
		}

		*x = tx + wc->hwidth;
		*y = ty + wc->hheight;

		return TRUE;
	} else {
		*x = wx;
		*y = wy;

		return FALSE;
	}
}

static void
get_pixel (guchar *buf, int rowstride, int x, int y, guchar *p)
{
	buf += y * rowstride + x * 3;
	p[0] = buf[0];
	p[1] = buf[1];
	p[2] = buf[2];
}

static void
whirl_context_whirl (WhirlContext *wc, guchar *in_buf, guchar *out_buf)
{
	guchar *dest1, *dest2;
	guchar *p1, *p2;
	int row, col;
	int x, y;

	dest1 = out_buf;
	dest2 = out_buf + wc->height * wc->rowstride;

	compute_trig_lut (wc, global_angle / 180.0 * M_PI);

	for (row = 0; row <= wc->height / 2; row++) {
		p1 = dest1;
		p2 = dest2 - 3;

		for (col = 0; col < wc->width; col++) {
			if (get_whirl_coords (wc, col, row, &x, &y)) {
				get_pixel (in_buf, wc->rowstride, x, y, p1);

				x = wc->width - 1 - x;
				y = wc->height - 1 - y;

				get_pixel (in_buf, wc->rowstride, x, y, p2);
			} else {
				get_pixel (in_buf, wc->rowstride, col, row, p1);
				get_pixel (in_buf, wc->rowstride,
					   wc->width - col - 1,
					   wc->height - row - 1,
					   p2);
			}

			p1 += 3;
			p2 -= 3;
		}

		dest1 += wc->rowstride;
		dest2 -= wc->rowstride;
	}
}

void
set_size (int width, int height, int rowstride)
{
	the_wc = whirl_context_new (width, height, rowstride);
}

void
set_params (const char *str)
{
	global_angle = atof (str);
}

char *
get_params (void)
{
	return g_strdup_printf ("%f", global_angle);
}

void 
process_frame (void *in_addr, void *out_addr)
{
	whirl_context_whirl (the_wc, in_addr, out_addr);
}
