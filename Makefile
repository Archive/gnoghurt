EXTRA_FLAGS=-Wall -O6 -mpentiumpro -funroll-loops -finline-functions -ffast-math 
#EXTRA_FLAGS=-g -Wall

########################################################
# You should not need to edit below this line
########################################################

# The folliwng
#
# ORBIT_CFLAGS=`orbit-config --cflags`
# ORBIT_LIBS=`orbit-config --libs server`
# GTK_CFLAGS=`gtk-config --cflags`
# GTK_LIBS=`gtk-config --libs`

GNOME_CFLAGS = -Wall -g `gnome-config --cflags gnomeui gnorba` -I../vidserver
GNOME_LIBS = -g `gnome-config --libs gnomeui gnorba` 

CFLAGS=$(EXTRA_FLAGS) $(GNOME_CFLAGS)
LIBS=-Wall $(ORBIT_LIBS) $(GNOME_LIBS)

COMMON_OBJS=					\
	VidServer-common.o			\
	VidServer-skels.o			\
	VidServer-stubs.o			\
	server.o				\
	client.o				\
	utils.o

EDITOR_OBJS = 					\
	filter-edit.o				\
	filters.o				\
	main.o					\
	tools.o					\
	video-controls.o      			\
	whirl-controls.o     			\
	$(COMMON_OBJS)

BINS=						\
	commandline 				\
	echo					\
	echo-filter 				\
	edge-filter 				\
	edge 					\
	emboss					\
	emboss-filter 				\
	filter-edit				\
	invert					\
	invert-filter 				\
	monitor					\
	testserver				\
	videoserver 				\
	whirl-filter 				\
	whirl

all: VidServer.h $(BINS)

VidServer-common.c VidServer-skels.c VidServer-stubs.c VidServer.h: VidServer.idl
	orbit-idl VidServer.idl

clean:
	rm -rf *.o $(BINS) *-skels.c *-common.c *-stubs.c VidServer.h

videoserver.o: VidServer.h

videoserver: videoserver.o $(COMMON_OBJS)
	$(CC) -o $@ videoserver.o $(COMMON_OBJS) $(LIBS)

testserver: testserver.o $(COMMON_OBJS)
	$(CC) -o $@ testserver.o $(COMMON_OBJS) $(LIBS)

whirl: whirl-client.o clientmain.o $(COMMON_OBJS)
	$(CC) -o $@ whirl-client.o clientmain.o $(COMMON_OBJS) $(LIBS)

whirl-sound: whirl-sound.o clientmain.o $(COMMON_OBJS)
	$(CC) -o $@ whirl-sound.o clientmain.o $(COMMON_OBJS) $(LIBS) -lesd

whirl-filter: whirl-filter.o filtermain.o $(COMMON_OBJS)
	$(CC) -o $@ whirl-filter.o filtermain.o $(COMMON_OBJS) $(LIBS)

invert: invert-client.o clientmain.o $(COMMON_OBJS)
	$(CC) -o $@ invert-client.o clientmain.o $(COMMON_OBJS) $(LIBS)

invert-filter: invert-filter.o filtermain.o $(COMMON_OBJS)
	$(CC) -o $@ invert-filter.o filtermain.o $(COMMON_OBJS) $(LIBS)

monitor: monitor.o $(COMMON_OBJS)
	$(CC) -o $@ monitor.o $(COMMON_OBJS) $(LIBS)

commandline: commandline.o $(COMMON_OBJS)
	$(CC) -o $@ commandline.o $(COMMON_OBJS) $(LIBS)

edge: edge-client.o clientmain.o $(COMMON_OBJS)
	$(CC) -o $@ edge-client.o clientmain.o $(COMMON_OBJS) $(LIBS)

edge-filter: edge-filter.o filtermain.o $(COMMON_OBJS)
	$(CC) -o $@ edge-filter.o filtermain.o $(COMMON_OBJS) $(LIBS)

emboss: emboss-client.o clientmain.o $(COMMON_OBJS)
	$(CC) -o $@ emboss-client.o clientmain.o $(COMMON_OBJS) $(LIBS)

emboss-filter: emboss-filter.o filtermain.o $(COMMON_OBJS)
	$(CC) -o $@ emboss-filter.o filtermain.o $(COMMON_OBJS) $(LIBS)

echo: echo-client.o clientmain.o $(COMMON_OBJS)
	$(CC) -o $@ echo-client.o clientmain.o $(COMMON_OBJS) $(LIBS)

echo-filter: echo-filter.o filtermain.o $(COMMON_OBJS)
	$(CC) -o $@ echo-filter.o filtermain.o $(COMMON_OBJS) $(LIBS)

filter-edit: $(EDITOR_OBJS)
	$(CC) -o $@ $(EDITOR_OBJS) $(LIBS)
