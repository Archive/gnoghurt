/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <esd.h>
#include <stdio.h>
#include <math.h>
#include "vidserver.h"
#include "clientmain.h"
#include <glib.h>

#define RATE 44100

int soundfd=-1;

static void
value_changed (GtkAdjustment *adj, gpointer data)
{
	char buf[100];

	snprintf (buf, 100, "%g", adj->value);
	set_params (buf);
}

gboolean
sound_watch (GIOChannel *channel, GIOCondition condition, gpointer data)
{
	static gint16 aubuf[128000];
	gint i, count;
	float rms;
	GtkAdjustment *adj = data;

	/* grab 1/10 secound samples, computer RMS ampl */
	count = read( soundfd, aubuf, 2*RATE/10 );
    
	if (count < 0) {
		exit(1);
	}
	
	rms = 0.0;
	for (i=0; i < count; i++)
		rms += aubuf[i]*aubuf[i];
	
	rms = sqrt(rms/(float)count);

	gtk_adjustment_set_value (adj, rms/20);
}

GtkWidget *
create_controls (void)
{
	GtkAdjustment *adj;
	GtkWidget *scale;
	GIOChannel *channel;

	soundfd=esd_monitor_stream(ESD_BITS16|ESD_MONO|ESD_STREAM|ESD_PLAY,
				   RATE,NULL, "ampl");

	if (soundfd<0) {
		  fprintf(stderr, "Cannot connect to EsounD\n");
		  exit(1);
	}

	/* Create the controls */
	
	adj = GTK_ADJUSTMENT (gtk_adjustment_new (0.0, -720.0, 720.0, 1.0, 10.0, 10.0));
	gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
			    GTK_SIGNAL_FUNC (value_changed),
			    NULL);

	channel = g_io_channel_unix_new (soundfd);
	g_io_add_watch (channel, G_IO_IN | G_IO_HUP, sound_watch, adj);
	g_io_channel_unref (channel);
	
	scale = gtk_hscale_new (adj);

	return scale;
}

char *
get_filter_command (void)
{
	return "./whirl-filter";
}
