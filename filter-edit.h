/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#ifndef FILTER_EDIT_H
#define FILTER_EDIT_H

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-app.h>
#include <libgnomeui/gnome-canvas.h>
#include "filters.h"
#include "tools.h"
#include "vidserver.h"

BEGIN_GNOME_DECLS


#define TYPE_FILTER_EDIT            (filter_edit_get_type ())
#define FILTER_EDIT(obj)            (GTK_CHECK_CAST ((obj), TYPE_FILTER_EDIT, FilterEdit))
#define FILTER_EDIT_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), TYPE_FILTER_EDIT, FilterEditClass))
#define IS_FILTER_EDIT(obj)         (GTK_CHECK_TYPE ((obj), TYPE_FILTER_EDIT))
#define IS_FILTER_EDIT_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), TYPE_FILTER_EDIT))

/* Global ORB variable. BAD. FIXME. */
CORBA_ORB orb;

typedef struct _FilterEdit FilterEdit;
typedef struct _FilterEditClass FilterEditClass;

struct _FilterEdit {
	GnomeApp app;

	GtkWidget *canvas;
	GnomeCanvasGroup *link_group;
	GnomeCanvasGroup *filter_group;

	GtkWidget *move_button;
	GtkWidget *link_button;
	GtkWidget *delete_button;

	FilterSpec *current_spec;
	ToolEventFunc tool_func;
	gpointer tool_data;

	GList *filters;
};

struct _FilterEditClass {
	GnomeAppClass parent_class;
};


GtkType filter_edit_get_type (void);
GtkWidget *filter_edit_new (void);


END_GNOME_DECLS

#endif
