/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "vidserver.h"

#define WIDTH 640
#define HEIGHT 480
#define CHECK_SIZE 16

long width, height, rowstride;
long sequence = 0;
gchar color = 0;

gboolean 
write_frame (void *data)
{
  VidServer *server = data;
  char *buf;
  int x, y;
  int m;

  buf = vid_server_get_frame (server, ++sequence);

  for (y = 0; y < HEIGHT; y++) {
	  m = ((y / CHECK_SIZE) & 1);

	  for (x = 0; x < WIDTH; x++) {
#if 0
		  /* Grid */
		  *buf++ = (!(x % CHECK_SIZE) ^ !(y % CHECK_SIZE)) ? color : 255 - color;
		  *buf++ = (!(x % CHECK_SIZE) ^ !(y % CHECK_SIZE)) ? color : 255 - color;
		  *buf++ = (!(x % CHECK_SIZE) ^ !(y % CHECK_SIZE)) ? color : 255 - color;
#else
		  /* Checkerboard */

		  if (m == ((x / CHECK_SIZE) & 1)) {
			  *buf++ = color;
			  *buf++ = color;
			  *buf++ = color;
		  } else {
			  *buf++ = 255 - color;
			  *buf++ = 255 - color;
			  *buf++ = 255 - color;
		  }
#endif
	  }
  }

  color += 10;

  vid_server_write_frame (server);

  return TRUE;
}

int 
main (int argc, char **argv)
{
  GMainLoop *loop;
  
  CORBA_ORB orb;
  CORBA_Environment ev;
  
  VidServer *server;

  CORBA_exception_init(&ev);
  orb = vid_init (&argc, &argv);

  server = vid_server_create (orb, WIDTH, HEIGHT, WIDTH * 3, NULL, NULL, NULL, NULL, NULL);
  vid_server_write_ior (server);

  g_timeout_add (20, write_frame, server);
  
  loop = g_main_new (FALSE);
  g_main_run (loop);
  g_main_destroy (loop);

  vid_server_unref (server);

  return 0;
}


