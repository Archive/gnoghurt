/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include "clientmain.h"
#include "vidserver.h"
#include <stdio.h>
#include <gdk/gdkx.h>
#include <X11/extensions/XShm.h>

typedef struct _FilterInfo FilterInfo;

int color = 254 << 7;
int brightness = 128 << 8;
int hue = 128 << 8;
int contrast = 0xd8 << 7;

char *paramstr = NULL;

struct _FilterInfo {
  VidChannel *channel;
  VidServer_Server server;
  char *addr;
  long seqno;
};

static void frame_received (long seqno, char *addr, void *data);
static void destroy_func (void *data);

int frames;

int width, height, rowstride;
GtkWidget *darea;

gboolean backing_store = TRUE;

#define MAX_FILTERS 16

FilterInfo filters[MAX_FILTERS];
int n_filters = 0;

CORBA_ORB orb;
VidServer_Server videoserver_channel;
VidServer_Server videoserver_server;

void
set_params (char *str)
{
  CORBA_Environment ev;
  int i, len;

  g_return_if_fail (str != NULL);
  
  CORBA_exception_init(&ev);

  if (paramstr)
    g_free (paramstr);

  len = strlen(str);
  paramstr = g_malloc (len + 2);
  strcpy (paramstr, str);
  paramstr[len] = '\n';
  paramstr[len+1] = '\0';

  for (i=0; i<n_filters; i++)
    {
      VidServer_Server_set_params (filters[i].server, paramstr, &ev);
    }
}

static void
set_n_filters (int new_n)
{
  int i;
  
  if (new_n < n_filters)
    {
      for (i=new_n; i<n_filters; i++)
	vid_channel_close (filters[i].channel);
    }
  else if (new_n > n_filters)
    {
      for (i=n_filters; i<new_n; i++)
	{
	  VidServer_Channel chobj;
	  CORBA_Environment ev;

	  char *args[2];
	  args[0] = get_filter_command ();
	  args[1] = NULL;
	  
	  CORBA_exception_init (&ev);
	  
	  filters[i].server = vid_run_server (orb, get_filter_command());

	  VidServer_Server_add_source (filters[i].server, videoserver_channel, &ev);
	  if (ev._major != CORBA_NO_EXCEPTION)
	    g_error ("Could not connect filters\n");

	  chobj = VidServer_Server_create_channel (filters[i].server, &ev);
	  if (ev._major != CORBA_NO_EXCEPTION)
	    g_error ("Could not create channel\n");
	  
	  filters[i].channel = vid_channel_open (orb, chobj,
						 &width, &height, &rowstride,
						 frame_received, destroy_func,
						 &filters[i]);

	  CORBA_Object_release (chobj, &ev);

	  if (paramstr)
	    VidServer_Server_set_params (filters[i].server, paramstr, &ev);
	}
    }

  n_filters = new_n;
}

/* Render a 24-bit RGB image in buf into the GdkImage, without dithering.
   This assumes native byte ordering - what should really be done is to
   check whether static_image->byte_order is consistent with the _ENDIAN
   config flag, and if not, use a different function.

   This one is even faster than the one below - its inner loop loads 3
   words (i.e. 4 24-bit pixels), does a lot of shifting and masking,
   then writes 2 words. */
static void
rgb_convert_565 (guchar *buf, guchar *obuf)
{
  int x, y;
  guchar *obptr;
  gint bpl;
  guchar *bptr, *bp2;
  guchar r, g, b;

  bptr = buf;
  bpl = width*2;
  for (y = 0; y < height; y++)
    {
      bp2 = bptr;
      obptr = obuf;
      if (((unsigned long)obuf | (unsigned long) bp2) & 3)
	{
	  for (x = 0; x < width; x++)
	    {
	      b = *bp2++;
	      g = *bp2++;
	      r = *bp2++;
	      ((guint16 *)obptr)[0] = ((r & 0xf8) << 8) |
		((g & 0xfc) << 3) |
		(b >> 3);
	      obptr += 2;
	    }
	}
      else
	{
	  for (x = 0; x < width - 3; x += 4)
	    {
	      guint32 b1r0g0b0;
	      guint32 g2b2r1g1;
	      guint32 r3g3b3r2;

	      b1r0g0b0 = ((guint32 *)bp2)[0];
	      g2b2r1g1 = ((guint32 *)bp2)[1];
	      r3g3b3r2 = ((guint32 *)bp2)[2];
	      ((guint32 *)obptr)[0] =
		((b1r0g0b0 & 0xf8) >> 3) |        /* 3  - 0  */
		((b1r0g0b0 & 0xfc00) >> 5) |      /* 10 - 5 */
		((b1r0g0b0 & 0xf80000) >> 8) |    /* 19 - 11 */
		((b1r0g0b0 & 0xf8000000) >> 11) | /* 27 - 16 */
		((g2b2r1g1 & 0xfc) << 19) |       /* 2  - 21 */
		((g2b2r1g1 & 0xf800) << 16);      /* 11 - 27 */
	      ((guint32 *)obptr)[1] =
		((g2b2r1g1 & 0xf80000) >> 19) |   /* 19 - 0 */
		((g2b2r1g1 & 0xfc000000) >> 21) | /* 26 - 5 */
		((r3g3b3r2 & 0xf8)  << 8) |       /* 3 - 11 */
		((r3g3b3r2 & 0xf800) << 5) |      /* 11 - 16 */
		((r3g3b3r2 & 0xfc0000) << 3) |    /* 18 - 21 */
		(r3g3b3r2 & 0xf8000000);          /* 27 - 27 */
	      bp2 += 12;
	      obptr += 8;
	    }
	  for (; x < width; x++)
	    {
	      r = *bp2++;
	      g = *bp2++;
	      b = *bp2++;
	      ((guint16 *)obptr)[0] = ((r & 0xf8) << 8) |
		((g & 0xfc) << 3) |
		(b >> 3);
	      obptr += 2;
	    }
	}
      bptr += rowstride;
      obuf += bpl;
    }
}

gboolean
draw_frame (gpointer data)
{
  FilterInfo *info = data;
  static GdkPixmap *bg_pixmap = NULL;
  static gchar *obuf;

  if (GTK_WIDGET_REALIZED (darea))
    {
      if (!bg_pixmap)
	{
	  GdkImage *out_image;
	  XShmSegmentInfo *x_shm_info;
	  Pixmap out_pmap;
	  
	  out_image = gdk_image_new (GDK_IMAGE_FASTEST, gdk_visual_get_system(),
				     width, height);
	  
	  x_shm_info = ((GdkImagePrivate *)out_image)->x_shm_info;
	  
	  out_pmap = XShmCreatePixmap (GDK_DISPLAY(), GDK_ROOT_WINDOW(),
				       out_image->mem,
				       x_shm_info,
				       width, height,
				       gdk_visual_get_system()->depth);

	  bg_pixmap = gdk_pixmap_foreign_new (out_pmap);
	  gdk_window_set_back_pixmap (darea->window, bg_pixmap, FALSE);
	  obuf = out_image->mem;
	}

      gdk_flush();
      rgb_convert_565 (info->addr, obuf);
      gdk_window_clear (darea->window);
    }
  
  vid_channel_frame_done (info->channel);

  return FALSE;
}

static void 
frame_received (long seqno, char *addr, void *data)
{
  FilterInfo *info = data;
  static long last_seqno = -1;

  if (last_seqno != -1 && seqno < last_seqno)
    {
      /* Just discard out-of-order frames for now */
      vid_channel_frame_done (info->channel);
      return;
    }

  frames++;

  last_seqno = seqno;

  info->addr = addr;
  info->seqno = seqno;
  gtk_idle_add (draw_frame, info);
}

static void 
destroy_func (void *data)
{
  gtk_main_quit();
}

static gboolean
fps_timeout (gpointer data)
{
  GtkWidget *label = data;
  char *text;

  text = g_strdup_printf ("%d", frames);
  gtk_label_set_text (GTK_LABEL (label), text);
  g_free (text);

  frames = 0;

  return TRUE;
}

static void
spin_changed (GtkAdjustment *adj)
{
  set_n_filters ((int)(adj->value + 0.5));
}

static void
set_pic_params ()
{
  CORBA_Environment ev;
  char *params;

  CORBA_exception_init(&ev);

  params = g_strdup_printf("%d %d %d %d", brightness, hue, color, contrast);
  VidServer_Server_set_params (videoserver_server, params, &ev);
  g_free (params);
}

static void
contrast_changed (GtkAdjustment *adj)
{
  contrast = (adj->value * 65535)/100;
  set_pic_params();
}

static void
brightness_changed (GtkAdjustment *adj)
{
  brightness = (adj->value * 65535)/100;
  set_pic_params();
}

static GtkWidget *
create_slider (const char *name, GtkSignalFunc cb)
{
  GtkAdjustment *adj;

  GtkWidget *hbox;
  GtkWidget *slider;
  GtkWidget *label;

  hbox = gtk_hbox_new (FALSE, 5);

  label = gtk_label_new (name);
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  adj = GTK_ADJUSTMENT (gtk_adjustment_new (50., 0, 100., 1., 10., 10.));
  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      GTK_SIGNAL_FUNC (cb), NULL);

  slider = gtk_hscale_new (adj);
  gtk_box_pack_start (GTK_BOX (hbox), slider, TRUE, TRUE, 0);

  return hbox;
}

static void
create_gui (void)
{
  GtkAdjustment *adj;

  GtkWidget *slider;
  GtkWidget *window;
  GtkWidget *hbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *spinbutton;
  GtkWidget *controls;
  GtkWidget *hseparator;

  gdk_rgb_init();

  gtk_widget_push_visual (gdk_rgb_get_visual());
  gtk_widget_push_colormap (gdk_rgb_get_cmap());

  window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

  gtk_signal_connect (GTK_OBJECT (window), "destroy",
		      GTK_SIGNAL_FUNC (gtk_main_quit), NULL);

  vbox = gtk_vbox_new (FALSE, 4);
  gtk_container_add (GTK_CONTAINER (window), vbox);

  hbox = gtk_hbox_new (FALSE, 0);
  gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

  label = gtk_label_new ("# Filters: ");
  gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  adj = GTK_ADJUSTMENT (gtk_adjustment_new (n_filters, 0, MAX_FILTERS, 1., 1., 1.));
  
  spinbutton = gtk_spin_button_new (adj, 0, 0);

  gtk_signal_connect (GTK_OBJECT (adj), "value_changed",
		      GTK_SIGNAL_FUNC (spin_changed), NULL);

  gtk_box_pack_start (GTK_BOX (hbox), spinbutton, FALSE, FALSE, 0);
  
  label = gtk_label_new ("0");
  gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);
  gtk_timeout_add (1000, fps_timeout, label);
  
  label = gtk_label_new ("Frames / second: ");
  gtk_box_pack_end (GTK_BOX (hbox), label, FALSE, FALSE, 0);

  slider = create_slider ("Contrast: ", contrast_changed);
  gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);

  slider = create_slider ("Brightness: ", brightness_changed);
  gtk_box_pack_start (GTK_BOX (vbox), slider, FALSE, FALSE, 0);

  hseparator = gtk_hseparator_new ();
  gtk_box_pack_start (GTK_BOX (vbox), hseparator, FALSE, FALSE, 0);

  controls = create_controls();
  gtk_box_pack_start (GTK_BOX (vbox), controls, FALSE, FALSE, 0);

  darea = gtk_drawing_area_new ();
  gtk_widget_set_usize (darea, width, height);

  gtk_box_pack_start (GTK_BOX (vbox), darea, FALSE, FALSE, 0);

  gtk_widget_show_all (window);
}

int 
main (int argc, char **argv)
{
  CORBA_Environment ev;

  orb = vid_init (&argc, &argv);
  gtk_init (&argc, &argv);

  CORBA_exception_init(&ev);
  
  videoserver_server = vid_server_ior_read (orb);

  videoserver_channel = VidServer_Server_create_channel (videoserver_server, &ev);
  if (ev._major != CORBA_NO_EXCEPTION)
    g_error ("Could not create channel\n");

  set_n_filters (1);

  create_gui();
  gtk_main ();
  set_n_filters (0);

  VidServer_Channel_destroy (videoserver_channel, &ev);
  CORBA_Object_release (videoserver_channel, &ev);
  CORBA_Object_release (videoserver_server, &ev);

  return 0;
}
