/* Gnoghurt - the RHAD Labs Video Filter Software
 * Copyright (C) 1999 Red Hat, Inc.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */
#include <gnome.h>
#include "filter-edit.h"
#include <libgnorba/gnorba.h>


static void
destroy_cb (GtkObject *object, gpointer data)
{
	gtk_main_quit ();
}

int
main (int argc, char **argv)
{
	GtkWidget *fe;
	CORBA_Environment ev;
	CORBA_exception_init (&ev);

	orb = gnome_CORBA_init ("filter-edit", "0.1", &argc, argv, 0, &ev);

	fe = filter_edit_new ();
	gtk_signal_connect (GTK_OBJECT (fe), "destroy",
			    GTK_SIGNAL_FUNC (destroy_cb),
			    NULL);

	gtk_widget_show_all (fe);
	gtk_main ();
	return 0;
}
